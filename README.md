# sicpa



## Requirements

- [Flutter 2.5.0+ (stable channel)](https://flutter.dev/docs/get-started/install)
- [Dart 2.13.4+](https://github.com/dart-lang/sdk/wiki/Installing-beta-and-dev-releases-with-brew,-choco,-and-apt-get#installing)

## Environment

**iOS**

- iOS 13+

**Android**

- Android 5.1+
    - minSdkVersion 16
- targetSdkVersion 30

![](https://bloclibrary.dev/assets/bloc_architecture_full.png)

## App architecture

- Base on [BLoC (Business Logic Component)](https://bloclibrary.dev/#/architecture)

## Code Style
- [Effective Dart](https://dart.dev/guides/language/effective-dart)


## Models
- Use [Freezed](https://pub.dev/packages/freezed)





## Code collections


#### Architecture

|Working status|Category|Description|Codes|
|:---:|---|---|---|
| ✅ | Base | Using [flutter_bloc](https://pub.dev/packages/flutter_bloc) | blocs |
| ✅ | Networking | Using [http](https://pub.dev/packages/http) | apiDataProvider.dart |
| ✅ | Data | Using [Freezed](https://pub.dev/packages/freezed) | [model classes]() |
| ✅ | Constants | Define constants  | [constants.dart]() |





#### Testing
|Working status|Category|Description|Codes|
|:---:|---|---|---|
| ✅ | Data(Api) | Using [Mockito](https://pub.dev/packages/mockito) | |
| ✅ | Models | Using [flutter_test](https://api.flutter.dev/flutter/flutter_test/flutter_test-library.html) | |
| ✅ | Repositories | Using [flutter_test](https://api.flutter.dev/flutter/flutter_test/flutter_test-library.html) | |
| ✅ | Blocs | Using [Mockito](https://pub.dev/packages/mockito) | |



## Getting Started



### Setup

```
$ flutter pub get
$ flutter pub run build_runner build --delete-conflicting-outputs
```

### Test
```
$ flutter test
```

### Make .apk file

Android
```
$ flutter build apk --debug
```

**Contributors**
- [Michael](https://bitbucket.org/tyfoo16/)