// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:sicpa/UI/screens/home_screen.dart';
import 'package:sicpa/UI/screens/search_screen.dart';

import 'package:sicpa/main.dart';

void main() {
  testWidgets('Home Screen - Tap Search Article navigate SearchScreen',
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    expect(find.byType(ListTile), findsWidgets);
    await tester.tap(find.text("Search Articles"));
    await tester.pumpAndSettle();
    expect(find.byType(SearchScreen), findsOneWidget);
  });

  testWidgets('HomeScreen - All required UI exists.',
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());

    var searchArticlesListTile = find.text("Search Articles");
    expect(searchArticlesListTile, findsOneWidget);

    var mostViewedListTile = find.text("Most Viewed");
    expect(mostViewedListTile, findsOneWidget);

    var mostSharedListTile = find.text("Most Shared");
    expect(mostSharedListTile, findsOneWidget);

    var mostEmailedListTile = find.text("Most Emailed");
    expect(mostEmailedListTile, findsOneWidget);

    expect(find.byType(HomeScreen), findsOneWidget);
  });

  testWidgets('SearchScreen - required UI exists', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    expect(find.byType(ListTile), findsWidgets);
    await tester.tap(find.text("Search Articles"));
    await tester.pumpAndSettle();
    expect(find.byType(SearchScreen), findsOneWidget);

    var searchTxtField = find.byType(TextFormField);
    expect(searchTxtField, findsOneWidget);

    var submitButton = find.byType(ElevatedButton);
    expect(submitButton, findsOneWidget);
  });

  testWidgets(
      'SearchScreen - enter BabyShark in text field, return Error Widget.',
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    expect(find.byType(ListTile), findsWidgets);
    await tester.tap(find.text("Search Articles"));
    await tester.pumpAndSettle();
    expect(find.byType(SearchScreen), findsOneWidget);

    var searchTxtField = find.byType(TextFormField);
    expect(searchTxtField, findsOneWidget);

    var submitButton = find.byType(ElevatedButton);
    expect(submitButton, findsOneWidget);

    await tester.tap(searchTxtField);
    await tester.enterText(searchTxtField, 'BabyShark');
    expect(find.text("BabyShark"), findsOneWidget);

    await tester.tap(submitButton);
    await tester.pumpAndSettle();

    expect(find.byKey(Key("error_widget")), findsOneWidget);
  });
}
