import 'package:sicpa/data/apiDataProvider.dart';
import 'package:sicpa/models/article.dart';
import 'package:sicpa/models/facet_fields.dart';
import 'package:sicpa/models/popular_article.dart';
import 'package:sicpa/models/period.dart';
import 'package:sicpa/models/share_type.dart';
import 'package:sicpa/models/sort_method.dart';
import 'package:sicpa/repositeries/articles_repository.dart';
import 'package:test/test.dart';
import 'package:http/http.dart' as http;

void main() {
  group("Most Viewed Articles Test: ", () {
    test(
        "period 1 Day - return response type correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostViewedArticles(Period.OneDay);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      print("Rest 6 seconds");
      await Future.delayed(Duration(seconds: 6));
    });

    test("period 7 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostViewedArticles(Period.SevenDays);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      print("Rest 6 seconds");
      await Future.delayed(Duration(seconds: 6));
    });

    test(
        "period 30 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostViewedArticles(Period.ThirtyDays);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });
  });

  group("Most Emailed Articles Test: ", () {
    test(
        "period 1 Day - return response type correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostEmailedArticles(Period.OneDay);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });

    test("period 7 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostEmailedArticles(Period.SevenDays);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });

    test(
        "period 30 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostEmailedArticles(Period.ThirtyDays);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });
  });

  group("Most Shared Articles Test: ", () {
    test(
        "period 1 Day - return response type correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostSharedArticles(Period.OneDay);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });

    test("period 7 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostSharedArticles(Period.SevenDays);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });

    test(
        "period 30 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostSharedArticles(Period.ThirtyDays);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });
  });

  group("Most Shared Articles with Facebook ShareType Test: ", () {
    test(
        "period 1 Day - return response type correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostSharedArticles(Period.OneDay,
          shareType: ShareType.Facebook);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });

    test("period 7 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostSharedArticles(Period.SevenDays,
          shareType: ShareType.Facebook);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });

    test(
        "period 30 Days - return response correctly and matched results length",
        () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.fetchMostSharedArticles(Period.ThirtyDays,
          shareType: ShareType.Facebook);
      expect(res.results, isA<List<PopularArticle>>());
      expect(res.results.length, equals(res.resultsLength));
      await Future.delayed(Duration(seconds: 6));
    });
  });

  group("Search article with filter test: ", () {
    test("Range of date", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res =
          await repo.searchArticles(beginDate: "18500316", endDate: "19900515");
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("facet true", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facet: true);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("fcet false", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facet: false);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields DayOfWeek", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facetFields: FacetFields.DayOfWeek);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields DocumentType", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res =
          await repo.searchArticles(facetFields: FacetFields.DocumentType);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields Ingredients", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res =
          await repo.searchArticles(facetFields: FacetFields.Ingredients);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields NewsDesk", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facetFields: FacetFields.NewsDesk);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields PubMonth", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facetFields: FacetFields.PubMonth);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields PubYear", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facetFields: FacetFields.PubYear);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields SectionName", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res =
          await repo.searchArticles(facetFields: FacetFields.SectionName);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields SectionName", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(facetFields: FacetFields.Source);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields SubsectionName", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res =
          await repo.searchArticles(facetFields: FacetFields.SubsectionName);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("Facet_fields SubsectionName", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res =
          await repo.searchArticles(facetFields: FacetFields.TypeOfMaterial);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("sort with Newest", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(sort: SortMethod.Newest);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("sort with Oldest", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(sort: SortMethod.Oldest);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });

    test("sort with Relevance", () async {
      final client = new http.Client();
      final dataProvider = ApiDataProvider(client);
      final repo = ArticlesRepository(dataProvider);
      final res = await repo.searchArticles(sort: SortMethod.Relevance);
      expect(res.docs, isA<List<Article>>());
      await Future.delayed(Duration(seconds: 6));
    });
  });
}
