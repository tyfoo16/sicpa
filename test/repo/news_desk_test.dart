import 'package:flutter_test/flutter_test.dart';
import 'package:sicpa/data/apiDataProvider.dart';
import 'package:sicpa/models/article.dart';
import 'package:sicpa/models/news_desk.dart';
import 'package:sicpa/repositeries/articles_repository.dart';
import 'package:http/http.dart' as http;

void main() {
  final client = new http.Client();
  final dataProvider = ApiDataProvider(client);
  final repo = ArticlesRepository(dataProvider);

  //rate limits per API: 4,000 requests per day and 10 requests per minute. You should sleep 6 seconds between calls to avoid hitting the per minute rate limit
  group("Test every news_desk type", () {
    for (var i = 0; i < NewsDesk.values.length; i++) {
      var v = NewsDesk.values[i];
      test("Search with news_desk ${v.value} return correct article type.",
          () async {
        final res =
            await repo.searchArticles(filterQuery: 'news_desk:("${v.value}")');
        expect(res.docs, isA<List<Article>>());

        final isNewsDeskType = res.docs
            .every((e) => e.newsDesk!.toLowerCase() == v.value.toLowerCase());
        expect(isNewsDeskType, equals(true));

        print("Rest 6 seconds");
        await Future.delayed(Duration(seconds: 6));
      });
    }
  });
}
