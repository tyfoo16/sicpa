import 'package:sicpa/models/media.dart';
import 'package:sicpa/models/popular_article.dart';
import 'package:test/test.dart';

class MockEmailedArticle {
  Map<String, dynamic> json = {
    "uri": "nyt://article/f83114c7-f8d4-5d82-9fd3-85f30ea49afd",
    "url":
        "https://www.nytimes.com/2021/09/17/style/jen-psaki-biden-press-secretary.html",
    "id": 100000007966452,
    "asset_id": 100000007966452,
    "source": "New York Times",
    "published_date": "2021-09-17",
    "updated": "2021-09-21 17:58:35",
    "section": "Style",
    "subsection": "",
    "nytdsection": "style",
    "adx_keywords":
        "United States Politics and Government;News and News Media;Content Type: Personal Profile;Psaki, Jennifer R;Biden, Joseph R Jr;Obama, Barack",
    "column": null,
    "byline": "By Michael M. Grynbaum",
    "type": "Article",
    "title": "Bully Pulpit No More: Jen Psaki’s Turn at the Lectern",
    "abstract":
        "President Biden’s press secretary has tamped down the vitriol that colored news briefings during the Trump administration. She still may not answer your question, though.",
    "des_facet": [
      "United States Politics and Government",
      "News and News Media",
      "Content Type: Personal Profile"
    ],
    "org_facet": [],
    "per_facet": ["Psaki, Jennifer R", "Biden, Joseph R Jr", "Obama, Barack"],
    "geo_facet": [],
    "media": [
      {
        "type": "image",
        "subtype": "photo",
        "caption": "",
        "copyright": "Peter van Agtmael for The New York Times",
        "approved_for_syndication": 1,
        "media-metadata": [
          {
            "url":
                "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-thumbStandard.jpg",
            "format": "Standard Thumbnail",
            "height": 75,
            "width": 75
          },
          {
            "url":
                "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-mediumThreeByTwo210.jpg",
            "format": "mediumThreeByTwo210",
            "height": 140,
            "width": 210
          },
          {
            "url":
                "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-mediumThreeByTwo440.jpg",
            "format": "mediumThreeByTwo440",
            "height": 293,
            "width": 440
          }
        ]
      }
    ],
    "eta_id": 0
  };
}

void main() {
  group("Test Article: ", () {
    final article = PopularArticle.fromJson(MockEmailedArticle().json);

    test("is a type of Article", () {
      expect(article, isA<PopularArticle>());
    });
    test("uri return correct type and value", () {
      expect(article.uri, isA<String>());
      expect(article.uri, equals(article.uri));
    });

    test("url return correct type and value", () {
      expect(article.url, isA<String>());
      expect(article.url, equals(article.url));
    });

    test("id return correct type and value", () {
      expect(article.id, isA<int>());
      expect(article.id, equals(article.id));
    });

    test("asset_id return correct type and value", () {
      expect(article.assetId, isA<int>());
      expect(article.assetId, equals(article.assetId));
    });

    test("source return correct type and value", () {
      expect(article.source, isA<String>());
      expect(article.source, equals(article.source));
    });

    test("published_date return correct type and value", () {
      expect(article.publishedDate, isA<String>());
      expect(article.publishedDate, equals(article.publishedDate));
    });

    test("updated return correct type and value", () {
      expect(article.updated, isA<String>());
      expect(article.updated, equals(article.updated));
    });

    test("section return correct type and value", () {
      expect(article.section, isA<String>());
      expect(article.section, equals(article.section));
    });

    test("subsection return correct type and value", () {
      expect(article.subsection, isA<String>());
      expect(article.subsection, equals(article.subsection));
    });

    test("nytdsection return correct type and value", () {
      expect(article.nytdsection, isA<String>());
      expect(article.nytdsection, equals(article.nytdsection));
    });

    test("adx_keywords return correct type and value", () {
      expect(article.adxKeywords, isA<String>());
      expect(article.adxKeywords, equals(article.adxKeywords));
    });

    test("column(deprecated) return correct type and value", () {
      expect(article.column, isA<Null>());
      expect(article.column, equals(article.column));
    });

    test("byline return correct type and value", () {
      expect(article.byline, isA<String>());
      expect(article.byline, equals(article.byline));
    });

    test("type return correct type and value", () {
      expect(article.type, isA<String>());
      expect(article.type, equals(article.type));
    });

    test("title return correct type and value", () {
      expect(article.title, isA<String>());
      expect(article.title, equals(article.title));
    });

    test("title return correct type and value", () {
      expect(article.title, isA<String>());
      expect(article.title, equals(article.title));
    });

    test("abstract return correct type and value", () {
      expect(article.abstractStr, isA<String>());
      expect(article.abstractStr, equals(article.abstractStr));
    });

    test("des_facet return correct type and value", () {
      expect(article.desFacet, isA<List<String>>());
      expect(article.desFacet, equals(article.desFacet));
    });

    test("org_facet return correct type and value", () {
      expect(article.orgFacet, isA<List<String>>());
      expect(article.orgFacet, equals(article.orgFacet));
    });

    test("geo_facet return correct type and value", () {
      expect(article.geoFacet, isA<List<String>>());
      expect(article.geoFacet, equals(article.geoFacet));
    });

    test("per_facet return correct type and value", () {
      expect(article.perFacet, isA<List<String>>());
      expect(article.perFacet, equals(article.perFacet));
    });

    test("media return correct type and value", () {
      expect(article.media, isA<List<Media>>());
      expect(article.media, equals(article.media));
    });

    test("eta_id(deprecated) return correct type and value", () {
      expect(article.etaId, isA<int>());
      expect(article.etaId, equals(article.etaId));
    });
  });
}
