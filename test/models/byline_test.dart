import 'package:sicpa/models/byline.dart';
import 'package:sicpa/models/person.dart';
import 'package:test/test.dart';

void main() {
  final mockData = {
    "original": "By Tyler Kepner",
    "person": [
      {
        "firstname": "Tyler",
        "middlename": null,
        "lastname": "Kepner",
        "qualifier": null,
        "title": null,
        "role": "reported",
        "organization": "",
        "rank": 1
      }
    ],
    "organization": null
  };

  final mockByline = Byline.fromJson(mockData);
  test("original return correct value and type.", () {
    expect(mockByline.original, isA<String>());
    expect(mockByline.original, equals("By Tyler Kepner"));
  });

  test("organization return empty string if null", () {
    expect(mockByline.organization, isA<String>());
    expect(mockByline.organization, equals(""));
  });
  test("person return correct value and type.", () {
    expect(mockByline.person, isA<List<Person>>());
    expect(mockByline.person.length, equals(1));
    expect(mockByline.person.first.firstname, equals("Tyler"));
    expect(mockByline.person.first.middlename, equals(""));
    expect(mockByline.person.first.lastname, equals("Kepner"));
    expect(mockByline.person.first.qualifier, equals(""));
    expect(mockByline.person.first.title, equals(""));
    expect(mockByline.person.first.role, equals("reported"));
    expect(mockByline.person.first.organization, equals(""));
    expect(mockByline.person.first.rank, equals(1));
  });
}
