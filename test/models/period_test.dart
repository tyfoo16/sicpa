import 'package:sicpa/models/period.dart';
import 'package:test/test.dart';

void main() {
  test("return 1 for Period.OneDay", () {
    expect(Period.OneDay.days, equals(1));
  });

  test("return 7 for Period.OneDay", () {
    expect(Period.SevenDays.days, equals(7));
  });

  test("return 30 for Period.OneDay", () {
    expect(Period.ThirtyDays.days, equals(30));
  });
}
