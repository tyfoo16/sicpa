import 'package:sicpa/models/keyword.dart';
import 'package:test/test.dart';

void main() {
  final Map<String, dynamic> mockData = {
    "name": "subject",
    "value": "Smartphones",
    "rank": 1,
    "major": "N"
  };
  final mockKeyword = Keyword.fromJson(mockData);

  test("Name return value and type correctly", () {
    expect(mockKeyword.name, isA<String>());
    expect(mockKeyword.name, equals("subject"));
  });

  test("Value return value and type correctly", () {
    expect(mockKeyword.value, isA<String>());
    expect(mockKeyword.value, equals("Smartphones"));
  });

  test("Rank return value and type correctly", () {
    expect(mockKeyword.rank, isA<int>());
    expect(mockKeyword.rank, equals(1));
  });

  test("Major return value and type correctly", () {
    expect(mockKeyword.major, isA<String>());
    expect(mockKeyword.major, equals("N"));
  });
}
