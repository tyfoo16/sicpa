import 'package:sicpa/models/person.dart';
import 'package:test/test.dart';

void main() {
  final Map<String, dynamic> mockData = {
    "firstname": "Elian",
    "middlename": null,
    "lastname": "Peltier",
    "qualifier": null,
    "title": null,
    "role": "reported",
    "organization": "",
    "rank": 1
  };
  final mockPerson = Person.fromJson(mockData);

  test("firstname return type & value correctly", () {
    expect(mockPerson.firstname, isA<String>());
    expect(mockPerson.firstname, equals("Elian"));
  });

  test("middleName return Empty String if null", () {
    expect(mockPerson.middlename, equals(""));
  });

  test("lastname return type & value correctly", () {
    expect(mockPerson.lastname, isA<String>());
    expect(mockPerson.lastname, equals("Peltier"));
  });

  test("qualifier return Empty String if null", () {
    expect(mockPerson.qualifier, equals(""));
  });

  test("title return Empty String if null", () {
    expect(mockPerson.middlename, equals(""));
  });

  test("role return type & value correctly", () {
    expect(mockPerson.role, isA<String>());
    expect(mockPerson.role, equals("reported"));
  });

  test("organization return type & value correctly", () {
    expect(mockPerson.organization, isA<String>());
    expect(mockPerson.organization, equals(""));
  });

  test("rank return type & value correctly", () {
    expect(mockPerson.rank, isA<int>());
    expect(mockPerson.rank, equals(1));
  });
}
