import 'package:sicpa/models/media_metadata.dart';
import 'package:test/test.dart';

class MockMediaMetadata {
  final Map<String, dynamic> json = {
    "url":
        "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-thumbStandard.jpg",
    "format": "Standard Thumbnail",
    "height": 75,
    "width": 75
  };
}

void main() {
  test("return correct type and value", () {
    final mockData = MediaMetadata.fromJson(MockMediaMetadata().json);

    expect(mockData, isA<MediaMetadata>());
    expect(mockData.url, isA<String>());
    expect(mockData.format, isA<String>());
    expect(mockData.height, isA<int>());
    expect(mockData.width, isA<int>());
  });
}
