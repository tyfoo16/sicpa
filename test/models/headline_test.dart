import 'package:sicpa/models/headline.dart';
import 'package:test/test.dart';

void main() {
  final Map<String, dynamic> mockData = {
    "main": "5 Minutes That Will Make You Love Classical Music",
    "kicker": null,
    "content_kicker": null,
    "print_headline": null,
    "name": null,
    "seo": null,
    "sub": null
  };
  final mockHeadline = Headline.fromJson(mockData);

  test("main return correct value and type", () {
    expect(mockHeadline.main, isA<String>());
    expect(mockHeadline.main,
        equals("5 Minutes That Will Make You Love Classical Music"));
  });
  test("kicker return empty string if null", () {
    expect(mockHeadline.kicker, isA<String>());
    expect(mockHeadline.kicker, equals(""));
  });

  test("content_kicker return empty string if null", () {
    expect(mockHeadline.contentKicker, isA<String>());
    expect(mockHeadline.contentKicker, equals(""));
  });

  test("print_headline return empty string if null", () {
    expect(mockHeadline.printHeadline, isA<String>());
    expect(mockHeadline.printHeadline, equals(""));
  });

  test("name return empty string if null", () {
    expect(mockHeadline.name, isA<String>());
    expect(mockHeadline.name, equals(""));
  });

  test("seo return empty string if null", () {
    expect(mockHeadline.seo, isA<String>());
    expect(mockHeadline.seo, equals(""));
  });

  test("sub return empty string if null", () {
    expect(mockHeadline.sub, isA<String>());
    expect(mockHeadline.sub, equals(""));
  });
}
