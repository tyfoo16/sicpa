import 'package:sicpa/models/popular_article.dart';
import 'package:sicpa/models/popular_articles_response.dart';
import 'package:test/test.dart';

class MockPopularArticlesResponse {
  Map<String, dynamic> json = {
    "num_results": 1,
    "results": [
      {
        "uri": "nyt://article/f83114c7-f8d4-5d82-9fd3-85f30ea49afd",
        "url":
            "https://www.nytimes.com/2021/09/17/style/jen-psaki-biden-press-secretary.html",
        "id": 100000007966452,
        "asset_id": 100000007966452,
        "source": "New York Times",
        "published_date": "2021-09-17",
        "updated": "2021-09-21 17:58:35",
        "section": "Style",
        "subsection": "",
        "nytdsection": "style",
        "adx_keywords":
            "United States Politics and Government;News and News Media;Content Type: Personal Profile;Psaki, Jennifer R;Biden, Joseph R Jr;Obama, Barack",
        "column": null,
        "byline": "By Michael M. Grynbaum",
        "type": "Article",
        "title": "Bully Pulpit No More: Jen Psaki’s Turn at the Lectern",
        "abstract":
            "President Biden’s press secretary has tamped down the vitriol that colored news briefings during the Trump administration. She still may not answer your question, though.",
        "des_facet": [
          "United States Politics and Government",
          "News and News Media",
          "Content Type: Personal Profile"
        ],
        "org_facet": [],
        "per_facet": [
          "Psaki, Jennifer R",
          "Biden, Joseph R Jr",
          "Obama, Barack"
        ],
        "geo_facet": [],
        "media": [
          {
            "type": "image",
            "subtype": "photo",
            "caption": "",
            "copyright": "Peter van Agtmael for The New York Times",
            "approved_for_syndication": 1,
            "media-metadata": [
              {
                "url":
                    "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-thumbStandard.jpg",
                "format": "Standard Thumbnail",
                "height": 75,
                "width": 75
              },
              {
                "url":
                    "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-mediumThreeByTwo210.jpg",
                "format": "mediumThreeByTwo210",
                "height": 140,
                "width": 210
              },
              {
                "url":
                    "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-mediumThreeByTwo440.jpg",
                "format": "mediumThreeByTwo440",
                "height": 293,
                "width": 440
              }
            ]
          }
        ],
        "eta_id": 0
      },
    ],
  };
}

void main() {
  final mockRes =
      PopularArticlesReponse.fromJson(MockPopularArticlesResponse().json);
  test("return correct type and value", () {
    expect(mockRes.resultsLength, equals(1));
    expect(mockRes.results, isA<List<PopularArticle>>());
    expect(mockRes.resultsLength, equals(mockRes.results.length));
  });
}
