import 'package:flutter_test/flutter_test.dart';
import 'package:sicpa/models/article.dart';

final mockArticleData = {
  "abstract":
      "The push to make USB-C connectors standard on all mobile devices would primarily affect Apple, which uses proprietary technology for charging.",
  "web_url":
      "https://www.nytimes.com/2021/09/23/business/european-union-apple-charging-port.html",
  "snippet":
      "The push to make USB-C connectors standard on all mobile devices would primarily affect Apple, which uses proprietary technology for charging.",
  "lead_paragraph":
      "The European Union unveiled plans on Thursday to make USB-C connectors the standard charging port for all smartphones, tablets and other electronic devices sold across the bloc, an initiative it says will reduce environmental waste.",
  "print_page": "22",
  "source": "The New York Times",
  "multimedia": [
    {
      "rank": 0,
      "subtype": "xlarge",
      "caption": null,
      "credit": null,
      "type": "image",
      "url":
          "images/2021/09/23/business/23economy-briefing-eu-charger/merlin_195078531_1dd74f19-167a-4ccc-bfa8-9332018be47d-articleLarge.jpg",
      "height": 412,
      "width": 600,
      "legacy": {
        "xlarge":
            "images/2021/09/23/business/23economy-briefing-eu-charger/merlin_195078531_1dd74f19-167a-4ccc-bfa8-9332018be47d-articleLarge.jpg",
        "xlargewidth": 600,
        "xlargeheight": 412
      },
      "subType": "xlarge",
      "crop_name": "articleLarge"
    },
    {
      "rank": 0,
      "subtype": "popup",
      "caption": null,
      "credit": null,
      "type": "image",
      "url":
          "images/2021/09/23/business/23economy-briefing-eu-charger/merlin_195078531_1dd74f19-167a-4ccc-bfa8-9332018be47d-popup.jpg",
      "height": 446,
      "width": 650,
      "legacy": {},
      "subType": "popup",
      "crop_name": "popup"
    },
  ],
  "headline": {
    "main":
        "In a setback for Apple, the European Union seeks a common charger for all phones.",
    "kicker": null,
    "content_kicker": null,
    "print_headline": null,
    "name": null,
    "seo": null,
    "sub": null
  },
  "keywords": [
    {"name": "subject", "value": "Smartphones", "rank": 1, "major": "N"},
    {
      "name": "subject",
      "value": "Waste Materials and Disposal",
      "rank": 2,
      "major": "N"
    },
    {"name": "subject", "value": "Electronics", "rank": 3, "major": "N"},
    {
      "name": "organizations",
      "value": "European Commission",
      "rank": 4,
      "major": "N"
    },
    {"name": "organizations", "value": "Apple Inc", "rank": 5, "major": "N"},
    {
      "name": "organizations",
      "value": "European Union",
      "rank": 6,
      "major": "N"
    }
  ],
  "pub_date": "2021-09-23T11:30:07+0000",
  "document_type": "article",
  "news_desk": "Business",
  "section_name": "Business Day",
  "byline": {
    "original": "By Elian Peltier",
    "person": [
      {
        "firstname": "Elian",
        "middlename": null,
        "lastname": "Peltier",
        "qualifier": null,
        "title": null,
        "role": "reported",
        "organization": "",
        "rank": 1
      }
    ],
    "organization": null
  },
  "type_of_material": "News",
  "_id": "nyt://article/204077fd-62bd-5804-a05e-1c50842df8cd",
  "word_count": 545,
  "uri": "nyt://article/204077fd-62bd-5804-a05e-1c50842df8cd"
};
void main() {
  final mockArticle = Article.fromJson(mockArticleData);

  test("Return correct type", () {
    expect(mockArticle, isA<Article>());
  });
}
