import 'package:flutter_test/flutter_test.dart';
import 'package:sicpa/models/article.dart';
import 'package:sicpa/models/articles_ressponse.dart';

import 'article_test.dart';

void main() {
  final mockResData = {
    "response": {
      "docs": [
        mockArticleData,
        mockArticleData,
        mockArticleData,
      ],
      "meta": {"hits": 13628, "offset": 0, "time": 7},
    }
  };

  final mockArticleRes = ArticlesReponse.fromJson(mockResData);

  test("Return correct type and value.", () {
    expect(mockArticleRes, isA<ArticlesReponse>());
  });
}
