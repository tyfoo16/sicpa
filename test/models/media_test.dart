import 'package:sicpa/models/media.dart';
import 'package:sicpa/models/media_metadata.dart';
import 'package:test/test.dart';

class MockMedia {
  final Map<String, dynamic> json = {
    "type": "image",
    "subtype": "photo",
    "caption": "",
    "copyright": "Peter van Agtmael for The New York Times",
    "approved_for_syndication": 1,
    "media-metadata": [
      {
        "url":
            "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-thumbStandard.jpg",
        "format": "Standard Thumbnail",
        "height": 75,
        "width": 75
      },
      {
        "url":
            "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-mediumThreeByTwo210.jpg",
        "format": "mediumThreeByTwo210",
        "height": 140,
        "width": 210
      },
      {
        "url":
            "https://static01.nyt.com/images/2021/09/19/fashion/18PSAKI5/18PSAKI5-mediumThreeByTwo440.jpg",
        "format": "mediumThreeByTwo440",
        "height": 293,
        "width": 440
      }
    ],
  };
}

void main() {
  test(
    "return correct type and value",
    () {
      final mockData = Media.fromJson(MockMedia().json);

      expect(mockData, isA<Media>());
      expect(mockData.type, isA<String>());
      expect(mockData.subtype, isA<String>());
      expect(mockData.caption, isA<String>());
      expect(mockData.copyright, isA<String>());
      expect(mockData.approvedForSyndication, isA<int>());
      expect(mockData.mediaMetadata, isA<List<MediaMetadata>>());
    },
  );
}
