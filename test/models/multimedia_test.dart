import 'package:sicpa/models/multimedia.dart';
import 'package:test/test.dart';

void main() {
  final Map<String, dynamic> json = {
    "rank": 0,
    "subtype": "xlarge",
    "caption": null,
    "credit": null,
    "type": "image",
    "url":
        "images/2021/09/23/t-magazine/23tmag-puffybags-slide-FQ2O-copy/23tmag-puffybags-slide-FQ2O-copy-articleLarge.jpg",
    "height": 400,
    "width": 600,
    // "legacy": {
    //   "xlarge":
    //       "images/2021/09/23/t-magazine/23tmag-puffybags-slide-FQ2O-copy/23tmag-puffybags-slide-FQ2O-copy-articleLarge.jpg",
    //   "xlargewidth": 600,
    //   "xlargeheight": 400
    // },
    "crop_name": "articleLarge"
  };

  final mockData = Multimedia.fromJson(json);

  test("Rank return correct type and value", () {
    expect(mockData.rank, isA<int>());
    expect(mockData.rank, equals(0));
  });

  test("subtype return correct type and value", () {
    expect(mockData.subtype, isA<String>());
    expect(mockData.subtype, equals("xlarge"));
  });

  test("caption return correct type and value", () {
    expect(mockData.caption, isA<String>());
    expect(mockData.caption, equals(""));
  });

  test("credit return correct type and value", () {
    expect(mockData.credit, isA<String>());
    expect(mockData.credit, equals(""));
  });

  test("type return correct type and value", () {
    expect(mockData.type, isA<String>());
    expect(mockData.type, equals("image"));
  });

  test("url return correct type and value", () {
    expect(mockData.url, isA<String>());
    expect(
        mockData.url,
        equals(
            "images/2021/09/23/t-magazine/23tmag-puffybags-slide-FQ2O-copy/23tmag-puffybags-slide-FQ2O-copy-articleLarge.jpg"));
  });

  test("height return correct type and value", () {
    expect(mockData.height, isA<int>());
    expect(mockData.height, equals(400));
  });

  test("width return correct type and value", () {
    expect(mockData.width, isA<int>());
    expect(mockData.width, equals(600));
  });

  test("crop_name return correct type and value", () {
    expect(mockData.cropName, isA<String>());
    expect(mockData.cropName, equals("articleLarge"));
  });
}
