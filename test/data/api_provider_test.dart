import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sicpa/data/apiDataProvider.dart';
import 'package:sicpa/data/api_exceptions.dart';
import 'package:sicpa/data/constants.dart';

import 'api_provider_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  group('Test Api Request Exceptions', () {
    final url = Configs.baseUrl;
    Map<String, dynamic> params = {
      "api-key": Configs.apiKey,
    };
    final uri = Uri.https(url, "path", params);
    test('returns BadRequestException on Status 400', () async {
      final client = MockClient();
      final apiProvider = ApiDataProvider(client);

      when(client.get(uri))
          .thenAnswer((_) async => http.Response('Bad Request', 400));

      expect(apiProvider.get("path"),
          throwsA(isInstanceOf<BadRequestException>()));
    });

    test('returns UnauthorizedException on Status 401', () async {
      final client = MockClient();
      final apiProvider = ApiDataProvider(client);

      when(client.get(uri)).thenAnswer((_) async => http.Response('''{
    "fault": {
        "faultstring": "Invalid ApiKey",
        "detail": {
            "errorcode": "oauth.v2.InvalidApiKey"
        }
    }
}''', 401));

      expect(apiProvider.get("path"),
          throwsA(isInstanceOf<UnauthorizedException>()));
    });

    test('returns NotFoundException on Status 404', () async {
      final client = MockClient();
      final apiProvider = ApiDataProvider(client);

      when(client.get(uri))
          .thenAnswer((_) async => http.Response('404 page not found', 404));

      expect(
          apiProvider.get("path"), throwsA(isInstanceOf<NotFoundException>()));
    });

    test('returns InternalServerErrorException on Status 500', () async {
      final client = MockClient();
      final apiProvider = ApiDataProvider(client);

      when(client.get(uri))
          .thenAnswer((_) async => http.Response('Internal Server Error', 500));

      expect(apiProvider.get("path"),
          throwsA(isInstanceOf<InternalServerErrorException>()));
    });

    test('returns RequestException on Status 999', () async {
      final client = MockClient();
      final apiProvider = ApiDataProvider(client);

      when(client.get(uri))
          .thenAnswer((_) async => http.Response('Unknown Error', 999));

      expect(
          apiProvider.get("path"), throwsA(isInstanceOf<RequestException>()));
    });

    test('returns TooManyRequestException on Status 429', () async {
      final client = MockClient();
      final apiProvider = ApiDataProvider(client);

      when(client.get(uri))
          .thenAnswer((_) async => http.Response("Too many request", 429));

      expect(apiProvider.get("path"),
          throwsA(isInstanceOf<TooManyRequestException>()));
    });
  });
}
