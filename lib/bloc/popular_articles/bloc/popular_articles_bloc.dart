import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/articles_type.dart';
import 'package:sicpa/models/period.dart';
import 'package:sicpa/models/popular_article.dart';
import 'package:sicpa/repositeries/articles_repository.dart';

part 'popular_articles_event.dart';
part 'popular_articles_state.dart';
part 'popular_articles_bloc.freezed.dart';

class PopularArticlesBloc
    extends Bloc<PopularArticlesEvent, PopularArticlesState> {
  final ArticlesRepository repo;

  PopularArticlesBloc(this.repo) : super(_Initial()) {
    on<PopularArticlesEvent>(_onEvent);
  }

  Future<void> _onEvent(
      PopularArticlesEvent event, Emitter<PopularArticlesState> emit) {
    return event.map(started: (_) async {
      emit(PopularArticlesState.initial());
    }, fetchArticle: (event) async {
      emit(PopularArticlesState.loading());
      try {
        final type = event.type;
        final period = event.period;
        switch (type) {
          case ArticlesType.MostEmailed:
            final res = await repo.fetchMostEmailedArticles(period);
            final articles = res.results;
            emit(PopularArticlesState.fetchSuccess(articles));
            break;
          case ArticlesType.MostViewed:
            final res = await repo.fetchMostViewedArticles(period);
            final articles = res.results;
            emit(PopularArticlesState.fetchSuccess(articles));
            break;
          case ArticlesType.MostShared:
            final res = await repo.fetchMostSharedArticles(period);
            final articles = res.results;
            emit(PopularArticlesState.fetchSuccess(articles));
            break;
        }
      } catch (error) {
        emit(PopularArticlesState.fetchFailed(error.toString()));
      }
    });
  }
}
