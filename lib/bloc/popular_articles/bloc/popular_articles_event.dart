part of 'popular_articles_bloc.dart';

@freezed
class PopularArticlesEvent with _$PopularArticlesEvent {
  const factory PopularArticlesEvent.started() = _Started;
  const factory PopularArticlesEvent.fetchArticle(
      ArticlesType type, Period period) = _FetchArticle;
}
