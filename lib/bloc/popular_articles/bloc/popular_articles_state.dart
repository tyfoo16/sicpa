part of 'popular_articles_bloc.dart';

@freezed
class PopularArticlesState with _$PopularArticlesState {
  const factory PopularArticlesState.initial() = _Initial;
  const factory PopularArticlesState.loading() = _Loading;
  const factory PopularArticlesState.fetchSuccess(List<PopularArticle> list) =
      _FetchSuccess;

  const factory PopularArticlesState.fetchFailed(String error) = _FetchFailed;
}
