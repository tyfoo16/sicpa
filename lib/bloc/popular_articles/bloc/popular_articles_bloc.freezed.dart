// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'popular_articles_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PopularArticlesEventTearOff {
  const _$PopularArticlesEventTearOff();

  _Started started() {
    return const _Started();
  }

  _FetchArticle fetchArticle(ArticlesType type, Period period) {
    return _FetchArticle(
      type,
      period,
    );
  }
}

/// @nodoc
const $PopularArticlesEvent = _$PopularArticlesEventTearOff();

/// @nodoc
mixin _$PopularArticlesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(ArticlesType type, Period period) fetchArticle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(ArticlesType type, Period period)? fetchArticle,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchArticle value) fetchArticle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchArticle value)? fetchArticle,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PopularArticlesEventCopyWith<$Res> {
  factory $PopularArticlesEventCopyWith(PopularArticlesEvent value,
          $Res Function(PopularArticlesEvent) then) =
      _$PopularArticlesEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$PopularArticlesEventCopyWithImpl<$Res>
    implements $PopularArticlesEventCopyWith<$Res> {
  _$PopularArticlesEventCopyWithImpl(this._value, this._then);

  final PopularArticlesEvent _value;
  // ignore: unused_field
  final $Res Function(PopularArticlesEvent) _then;
}

/// @nodoc
abstract class _$StartedCopyWith<$Res> {
  factory _$StartedCopyWith(_Started value, $Res Function(_Started) then) =
      __$StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$StartedCopyWithImpl<$Res>
    extends _$PopularArticlesEventCopyWithImpl<$Res>
    implements _$StartedCopyWith<$Res> {
  __$StartedCopyWithImpl(_Started _value, $Res Function(_Started) _then)
      : super(_value, (v) => _then(v as _Started));

  @override
  _Started get _value => super._value as _Started;
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'PopularArticlesEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(ArticlesType type, Period period) fetchArticle,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(ArticlesType type, Period period)? fetchArticle,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchArticle value) fetchArticle,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchArticle value)? fetchArticle,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements PopularArticlesEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$FetchArticleCopyWith<$Res> {
  factory _$FetchArticleCopyWith(
          _FetchArticle value, $Res Function(_FetchArticle) then) =
      __$FetchArticleCopyWithImpl<$Res>;
  $Res call({ArticlesType type, Period period});
}

/// @nodoc
class __$FetchArticleCopyWithImpl<$Res>
    extends _$PopularArticlesEventCopyWithImpl<$Res>
    implements _$FetchArticleCopyWith<$Res> {
  __$FetchArticleCopyWithImpl(
      _FetchArticle _value, $Res Function(_FetchArticle) _then)
      : super(_value, (v) => _then(v as _FetchArticle));

  @override
  _FetchArticle get _value => super._value as _FetchArticle;

  @override
  $Res call({
    Object? type = freezed,
    Object? period = freezed,
  }) {
    return _then(_FetchArticle(
      type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as ArticlesType,
      period == freezed
          ? _value.period
          : period // ignore: cast_nullable_to_non_nullable
              as Period,
    ));
  }
}

/// @nodoc

class _$_FetchArticle implements _FetchArticle {
  const _$_FetchArticle(this.type, this.period);

  @override
  final ArticlesType type;
  @override
  final Period period;

  @override
  String toString() {
    return 'PopularArticlesEvent.fetchArticle(type: $type, period: $period)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FetchArticle &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.period, period) ||
                const DeepCollectionEquality().equals(other.period, period)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(period);

  @JsonKey(ignore: true)
  @override
  _$FetchArticleCopyWith<_FetchArticle> get copyWith =>
      __$FetchArticleCopyWithImpl<_FetchArticle>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(ArticlesType type, Period period) fetchArticle,
  }) {
    return fetchArticle(type, period);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(ArticlesType type, Period period)? fetchArticle,
    required TResult orElse(),
  }) {
    if (fetchArticle != null) {
      return fetchArticle(type, period);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchArticle value) fetchArticle,
  }) {
    return fetchArticle(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchArticle value)? fetchArticle,
    required TResult orElse(),
  }) {
    if (fetchArticle != null) {
      return fetchArticle(this);
    }
    return orElse();
  }
}

abstract class _FetchArticle implements PopularArticlesEvent {
  const factory _FetchArticle(ArticlesType type, Period period) =
      _$_FetchArticle;

  ArticlesType get type => throw _privateConstructorUsedError;
  Period get period => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FetchArticleCopyWith<_FetchArticle> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$PopularArticlesStateTearOff {
  const _$PopularArticlesStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Loading loading() {
    return const _Loading();
  }

  _FetchSuccess fetchSuccess(List<PopularArticle> list) {
    return _FetchSuccess(
      list,
    );
  }

  _FetchFailed fetchFailed(String error) {
    return _FetchFailed(
      error,
    );
  }
}

/// @nodoc
const $PopularArticlesState = _$PopularArticlesStateTearOff();

/// @nodoc
mixin _$PopularArticlesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<PopularArticle> list) fetchSuccess,
    required TResult Function(String error) fetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<PopularArticle> list)? fetchSuccess,
    TResult Function(String error)? fetchFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_FetchSuccess value) fetchSuccess,
    required TResult Function(_FetchFailed value) fetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_FetchSuccess value)? fetchSuccess,
    TResult Function(_FetchFailed value)? fetchFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PopularArticlesStateCopyWith<$Res> {
  factory $PopularArticlesStateCopyWith(PopularArticlesState value,
          $Res Function(PopularArticlesState) then) =
      _$PopularArticlesStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$PopularArticlesStateCopyWithImpl<$Res>
    implements $PopularArticlesStateCopyWith<$Res> {
  _$PopularArticlesStateCopyWithImpl(this._value, this._then);

  final PopularArticlesState _value;
  // ignore: unused_field
  final $Res Function(PopularArticlesState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$PopularArticlesStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'PopularArticlesState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<PopularArticle> list) fetchSuccess,
    required TResult Function(String error) fetchFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<PopularArticle> list)? fetchSuccess,
    TResult Function(String error)? fetchFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_FetchSuccess value) fetchSuccess,
    required TResult Function(_FetchFailed value) fetchFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_FetchSuccess value)? fetchSuccess,
    TResult Function(_FetchFailed value)? fetchFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements PopularArticlesState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res>
    extends _$PopularArticlesStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'PopularArticlesState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<PopularArticle> list) fetchSuccess,
    required TResult Function(String error) fetchFailed,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<PopularArticle> list)? fetchSuccess,
    TResult Function(String error)? fetchFailed,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_FetchSuccess value) fetchSuccess,
    required TResult Function(_FetchFailed value) fetchFailed,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_FetchSuccess value)? fetchSuccess,
    TResult Function(_FetchFailed value)? fetchFailed,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements PopularArticlesState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$FetchSuccessCopyWith<$Res> {
  factory _$FetchSuccessCopyWith(
          _FetchSuccess value, $Res Function(_FetchSuccess) then) =
      __$FetchSuccessCopyWithImpl<$Res>;
  $Res call({List<PopularArticle> list});
}

/// @nodoc
class __$FetchSuccessCopyWithImpl<$Res>
    extends _$PopularArticlesStateCopyWithImpl<$Res>
    implements _$FetchSuccessCopyWith<$Res> {
  __$FetchSuccessCopyWithImpl(
      _FetchSuccess _value, $Res Function(_FetchSuccess) _then)
      : super(_value, (v) => _then(v as _FetchSuccess));

  @override
  _FetchSuccess get _value => super._value as _FetchSuccess;

  @override
  $Res call({
    Object? list = freezed,
  }) {
    return _then(_FetchSuccess(
      list == freezed
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as List<PopularArticle>,
    ));
  }
}

/// @nodoc

class _$_FetchSuccess implements _FetchSuccess {
  const _$_FetchSuccess(this.list);

  @override
  final List<PopularArticle> list;

  @override
  String toString() {
    return 'PopularArticlesState.fetchSuccess(list: $list)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FetchSuccess &&
            (identical(other.list, list) ||
                const DeepCollectionEquality().equals(other.list, list)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(list);

  @JsonKey(ignore: true)
  @override
  _$FetchSuccessCopyWith<_FetchSuccess> get copyWith =>
      __$FetchSuccessCopyWithImpl<_FetchSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<PopularArticle> list) fetchSuccess,
    required TResult Function(String error) fetchFailed,
  }) {
    return fetchSuccess(list);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<PopularArticle> list)? fetchSuccess,
    TResult Function(String error)? fetchFailed,
    required TResult orElse(),
  }) {
    if (fetchSuccess != null) {
      return fetchSuccess(list);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_FetchSuccess value) fetchSuccess,
    required TResult Function(_FetchFailed value) fetchFailed,
  }) {
    return fetchSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_FetchSuccess value)? fetchSuccess,
    TResult Function(_FetchFailed value)? fetchFailed,
    required TResult orElse(),
  }) {
    if (fetchSuccess != null) {
      return fetchSuccess(this);
    }
    return orElse();
  }
}

abstract class _FetchSuccess implements PopularArticlesState {
  const factory _FetchSuccess(List<PopularArticle> list) = _$_FetchSuccess;

  List<PopularArticle> get list => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FetchSuccessCopyWith<_FetchSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$FetchFailedCopyWith<$Res> {
  factory _$FetchFailedCopyWith(
          _FetchFailed value, $Res Function(_FetchFailed) then) =
      __$FetchFailedCopyWithImpl<$Res>;
  $Res call({String error});
}

/// @nodoc
class __$FetchFailedCopyWithImpl<$Res>
    extends _$PopularArticlesStateCopyWithImpl<$Res>
    implements _$FetchFailedCopyWith<$Res> {
  __$FetchFailedCopyWithImpl(
      _FetchFailed _value, $Res Function(_FetchFailed) _then)
      : super(_value, (v) => _then(v as _FetchFailed));

  @override
  _FetchFailed get _value => super._value as _FetchFailed;

  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_FetchFailed(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FetchFailed implements _FetchFailed {
  const _$_FetchFailed(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'PopularArticlesState.fetchFailed(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FetchFailed &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @JsonKey(ignore: true)
  @override
  _$FetchFailedCopyWith<_FetchFailed> get copyWith =>
      __$FetchFailedCopyWithImpl<_FetchFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<PopularArticle> list) fetchSuccess,
    required TResult Function(String error) fetchFailed,
  }) {
    return fetchFailed(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<PopularArticle> list)? fetchSuccess,
    TResult Function(String error)? fetchFailed,
    required TResult orElse(),
  }) {
    if (fetchFailed != null) {
      return fetchFailed(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_FetchSuccess value) fetchSuccess,
    required TResult Function(_FetchFailed value) fetchFailed,
  }) {
    return fetchFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_FetchSuccess value)? fetchSuccess,
    TResult Function(_FetchFailed value)? fetchFailed,
    required TResult orElse(),
  }) {
    if (fetchFailed != null) {
      return fetchFailed(this);
    }
    return orElse();
  }
}

abstract class _FetchFailed implements PopularArticlesState {
  const factory _FetchFailed(String error) = _$_FetchFailed;

  String get error => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FetchFailedCopyWith<_FetchFailed> get copyWith =>
      throw _privateConstructorUsedError;
}
