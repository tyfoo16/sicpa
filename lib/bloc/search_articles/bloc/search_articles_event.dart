part of 'search_articles_bloc.dart';

@freezed
class SearchArticlesEvent with _$SearchArticlesEvent {
  const factory SearchArticlesEvent.started() = _Started;
  const factory SearchArticlesEvent.search(SearchForm form) = _Search;
}
