part of 'search_articles_bloc.dart';

@freezed
class SearchArticlesState with _$SearchArticlesState {
  const factory SearchArticlesState.initial() = _Initial;
  const factory SearchArticlesState.loading() = _Loading;
  const factory SearchArticlesState.fetchSuccess(List<Article> articles) =
      _FetchSuccess;

  const factory SearchArticlesState.fetchFailed(String error) = _FetchFailed;
}
