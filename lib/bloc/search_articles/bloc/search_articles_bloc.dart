import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/article.dart';
import 'package:sicpa/models/search_form.dart';
import 'package:sicpa/repositeries/articles_repository.dart';

part 'search_articles_event.dart';
part 'search_articles_state.dart';
part 'search_articles_bloc.freezed.dart';

class SearchArticlesBloc
    extends Bloc<SearchArticlesEvent, SearchArticlesState> {
  final ArticlesRepository repo;
  SearchArticlesBloc(this.repo) : super(_Initial()) {
    on<SearchArticlesEvent>(_onEvent);
  }

  Future<void> _onEvent(
      SearchArticlesEvent event, Emitter<SearchArticlesState> emit) {
    return event.map(
      started: (_) async {
        emit(SearchArticlesState.initial());
      },
      search: (event) async {
        emit(SearchArticlesState.loading());
        try {
          final res = await repo.searchArticles(
            query: event.form.query,
          );
          print(res.docs);
          emit(SearchArticlesState.fetchSuccess(res.docs));
          return;
        } catch (err) {
          emit(SearchArticlesState.fetchFailed(err.toString()));
          return;
        }
      },
    );
  }
}
