import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sicpa/UI/screens/home_screen.dart';
import 'package:sicpa/bloc/search_articles/bloc/search_articles_bloc.dart';
import 'package:sicpa/repositeries/articles_repository.dart';

import 'bloc/bloc_observer.dart';
import 'bloc/popular_articles/bloc/popular_articles_bloc.dart';
import 'data/apiDataProvider.dart';
import 'package:http/http.dart' as http;

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final dataProvider = ApiDataProvider(http.Client());
    final articlesRepo = ArticlesRepository(dataProvider);
    final searchArticleBloc = SearchArticlesBloc(articlesRepo);
    final popularArticlesBloc = PopularArticlesBloc(articlesRepo);

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => popularArticlesBloc,
        ),
        BlocProvider(
          create: (context) => searchArticleBloc,
        ),
      ],
      child: MaterialApp(
        home: HomeScreen(),
      ),
    );
  }
}
