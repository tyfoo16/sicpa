import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sicpa/bloc/popular_articles/bloc/popular_articles_bloc.dart';

import 'package:sicpa/models/articles_type.dart';
import 'package:sicpa/models/media.dart';
import 'package:sicpa/models/media_metadata.dart';
import 'package:sicpa/models/period.dart';
import 'package:sicpa/models/popular_article.dart';

class ArticlesScreen extends StatefulWidget {
  final ArticlesType? type;
  const ArticlesScreen({Key? key, this.type}) : super(key: key);

  @override
  _ArticlesScreenState createState() => _ArticlesScreenState();
}

class _ArticlesScreenState extends State<ArticlesScreen> {
  Period period = Period.OneDay;
  late ArticlesType type;
  List<Period> periodList = [
    Period.OneDay,
    Period.SevenDays,
    Period.ThirtyDays,
  ];
  @override
  void initState() {
    super.initState();
    if (widget.type != null) {
      type = widget.type!;
      fetchPopularArticles();
    }
  }

  void fetchPopularArticles() {
    BlocProvider.of<PopularArticlesBloc>(context)
        .add(PopularArticlesEvent.fetchArticle(type, this.period));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${type.value}"),
        actions: [
          PopupMenuButton<Period>(
            icon: Icon(Icons.sort_rounded),
            onSelected: (period) {
              this.period = period;
              fetchPopularArticles();
            },
            itemBuilder: (BuildContext context) {
              return periodList.map((Period choice) {
                return PopupMenuItem<Period>(
                  value: choice,
                  child: Text(choice.toString()),
                );
              }).toList();
            },
          )
        ],
      ),
      body: Container(
        child: BlocBuilder<PopularArticlesBloc, PopularArticlesState>(
          builder: (context, state) {
            return state.when(
                initial: () => Container(),
                loading: () => _buildLoading(),
                fetchSuccess: (list) => _buildArticleList(list),
                fetchFailed: (error) => _buildError(error));
          },
        ),
      ),
    );
  }

  Widget _buildLoading() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildArticleList(List<PopularArticle> list) {
    return Container(
      child: RefreshIndicator(
        onRefresh: () async {
          fetchPopularArticles();
          return;
        },
        child: ListView.builder(
            itemBuilder: (context, i) {
              return _buildArticleTile(list[i]);
            },
            itemCount: list.length),
      ),
    );
  }

  Widget _buildArticleTile(PopularArticle item) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.blueAccent,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: ExpansionTile(
          collapsedIconColor: Colors.black,
          iconColor: Colors.black,
          title: Text(
            item.title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.white),
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Published: " + item.publishedDate,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                "" + item.source,
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildMediaData(item.media),
            ),
            _buildLabel("Title:", item.title),
            _buildLabel("Abtract:", item.abstractStr),
            _buildLabel("Keyword:", item.adxKeywords),
            _buildLabel("Last Updated:", item.updated),
            _buildLabel("Section:", item.section),
            _buildLabel("Source:", item.source),
            _buildLabel("Type:", item.type),
            _buildLabel("Reference Url:", item.url),
            _buildLabel(
                "Org facet  :",
                item.orgFacet.fold(
                    "",
                    (previousValue, element) =>
                        previousValue + element + ", ")),
            _buildLabel(
                "Per facet  :",
                item.perFacet.fold(
                    "",
                    (previousValue, element) =>
                        previousValue + element + ", ")),
            _buildLabel(
                "Geo facet:",
                item.geoFacet.fold(
                    "",
                    (previousValue, element) =>
                        previousValue + element + ", ")),
            _buildLabel(
                "Des facet:",
                item.desFacet.fold(
                    "",
                    (previousValue, element) =>
                        previousValue + element + ", ")),
            _buildLabel("Author:", item.byline),
          ],
        ),
      ),
    );
  }

  Widget _buildLabel(String title, String content) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Text(
              content,
              maxLines: 3,
              style: TextStyle(color: Colors.white54),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMediaData(List<Media> medias) {
    return Container(
      height: 80,
      child: ListView.builder(
        itemBuilder: (context, i) {
          return _buildImages(medias[i].mediaMetadata);
        },
        itemCount: medias.length,
      ),
    );
  }

  Widget _buildImages(List<MediaMetadata> metadatas) {
    return Container(
      color: Colors.black12,
      height: 80,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, i) {
          return Container(
            child: Image.network(
              metadatas[i].url,
              fit: BoxFit.fitHeight,
              height: 80,
              width: 80,
              errorBuilder: (context, exception, stacktrace) {
                return Text("Load Failed");
              },
            ),
          );
        },
        itemCount: metadatas.length,
      ),
    );
  }

  Widget _buildError(String error) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.error_rounded,
              size: 40,
            ),
            Text(error),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                fetchPopularArticles();
              },
              child: Text("Refresh"),
            ),
          ],
        ),
      ),
    );
  }
}
