import 'dart:math';

import 'package:flutter/material.dart';
import 'package:sicpa/UI/screens/articles_screen.dart';
import 'package:sicpa/UI/screens/search_screen.dart';
import 'package:sicpa/models/articles_type.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildTitle("Search"),
            _buildNavButton("Search Articles", () {
              navigateTo(SearchScreen());
            }),
            SizedBox(height: 16),
            _buildTitle("Popular"),
            _buildNavButton("Most Viewed", () {
              navigateTo(ArticlesScreen(
                type: ArticlesType.MostViewed,
              ));
            }),
            _buildNavButton("Most Shared", () {
              navigateTo(ArticlesScreen(
                type: ArticlesType.MostShared,
              ));
            }),
            _buildNavButton("Most Emailed", () {
              navigateTo(ArticlesScreen(
                type: ArticlesType.MostEmailed,
              ));
            }),
          ],
        ),
      ),
    );
  }

  void navigateTo(Widget screen) async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return screen;
    }));
  }

  Widget _buildTitle(String name) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(8),
      child: Text(
        name,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _buildNavButton(String title, Function onTap) {
    return Container(
      decoration: BoxDecoration(
        border: Border.symmetric(
          horizontal: BorderSide(
            width: 0.2,
            color: Colors.black87,
          ),
        ),
      ),
      child: ListTile(
        onTap: () => onTap(),
        tileColor: Colors.primaries[Random().nextInt(Colors.primaries.length)],
        dense: true,
        title: Text(title),
        trailing: Icon(Icons.navigate_next),
      ),
    );
  }
}
