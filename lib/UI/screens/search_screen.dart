import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sicpa/bloc/search_articles/bloc/search_articles_bloc.dart';
import 'package:sicpa/models/article.dart';
import 'package:sicpa/models/media.dart';
import 'package:sicpa/models/media_metadata.dart';
import 'package:sicpa/models/multimedia.dart';
import 'package:sicpa/models/search_form.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  late TextEditingController searchCtrl;

  late SearchForm form;

  @override
  void initState() {
    searchCtrl = TextEditingController(text: "");

    form = SearchForm(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      "",
      null,
    );
    super.initState();
    initSearchState();
  }

  @override
  void dispose() {
    searchCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      floatingActionButton:
          BlocBuilder<SearchArticlesBloc, SearchArticlesState>(
        builder: (context, state) {
          return state.maybeWhen(
            initial: () => Container(),
            loading: () => Container(),
            orElse: () => FloatingActionButton(
              onPressed: () {
                initSearchState();
              },
              child: Icon(Icons.search),
              backgroundColor: Colors.amber,
            ),
          );
        },
      ),
      body: Container(
        child: Stack(
          children: [
            _buildSearchForm(),
            _buildArticleList(),
          ],
        ),
      ),
    );
  }

  Widget _buildSearchForm() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TextFormField(
            controller: searchCtrl,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.zero,
              border: UnderlineInputBorder(),
              labelText: "Search",
              helperText:
                  "Please key in any relevant keywords for your search.",
            ),
            onFieldSubmitted: (v) {
              print("Submitted");
              this.form = this.form.copyWith(query: v);
              print(this.form.query);
            },
            keyboardType: TextInputType.text,
          ),
          SizedBox(height: 32),
          ElevatedButton(
            onPressed: () {
              submitSearchForm();
            },
            child: Text("Submit"),
          ),
        ],
      ),
    );
  }

  void initSearchState() {
    BlocProvider.of<SearchArticlesBloc>(context)
        .add(SearchArticlesEvent.started());
  }

  void submitSearchForm() {
    BlocProvider.of<SearchArticlesBloc>(context)
        .add(SearchArticlesEvent.search(form));
  }

  Widget _buildArticleList() {
    return BlocBuilder<SearchArticlesBloc, SearchArticlesState>(
      builder: (context, state) {
        return state.when(
            initial: _buildInitial,
            loading: _buildLoading,
            fetchSuccess: _buildArticles,
            fetchFailed: _buildError);
      },
    );
  }

  Widget _buildInitial() {
    return Container();
  }

  Widget _buildLoading() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildArticles(List<Article> items) {
    return Container(
      key: Key("articles_list"),
      color: Colors.white,
      child: ListView.builder(
        itemBuilder: (context, i) {
          return _buildArticleTile(items[i]);
        },
        itemCount: items.length,
      ),
    );
  }

  Widget _buildArticleTile(Article item) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.blueAccent,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: ExpansionTile(
          collapsedIconColor: Colors.black,
          iconColor: Colors.black,
          title: Text(
            item.headline.main,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.white),
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Published: " + (item.pubDate ?? ""),
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildMediaData(item.multimedia),
            ),
            _buildLabel("Headline Main:", item.headline.main),
            _buildLabel("Word count:", item.wordCount.toString()),
            _buildLabel("Source:", item.source ?? ""),
            _buildLabel("Score:", item.score.toString()),
            _buildLabel(
                "Keywords:",
                item.keywords.fold(
                    "",
                    (previousValue, element) =>
                        previousValue + element.name + ", ")),
            _buildLabel("Original:", item.byline.original ?? ""),
            _buildLabel(
                "Persons:",
                item.byline.person.fold(
                    "",
                    (previousValue, element) =>
                        previousValue + (element.firstname ?? "") + ", ")),
          ],
        ),
      ),
    );
  }

  Widget _buildLabel(String title, String content) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Text(
              content,
              maxLines: 3,
              style: TextStyle(color: Colors.white54),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMediaData(List<Multimedia> medias) {
    return Container(
      height: 80,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, i) {
          return _buildImages(medias[i].url);
        },
        itemCount: medias.length < 5 ? medias.length : 5,
      ),
    );
  }

  Widget _buildImages(String? url) {
    if (url == null) {
      return Container();
    }
    return Container(
      color: Colors.black12,
      height: 80,
      child: Image.network(
        "https://nytimes.com/" + url,
        fit: BoxFit.fitHeight,
        height: 80,
        width: 80,
        filterQuality: FilterQuality.low,
        errorBuilder: (context, exception, stacktrace) {
          return Text("Load Failed");
        },
      ),
    );
  }

  Widget _buildError(String error) {
    return Container(
      key: Key("error_widget"),
      color: Colors.white,
      child: Center(child: Text(error)),
    );
  }
}
