import 'dart:convert';
import 'package:sicpa/data/apiDataProvider.dart';
import 'package:sicpa/models/articles_ressponse.dart';
import 'package:sicpa/models/facet_fields.dart';
import 'package:sicpa/models/popular_articles_response.dart';
import 'package:sicpa/models/period.dart';
import 'package:sicpa/models/share_type.dart';
import 'package:sicpa/models/sort_method.dart';

class ArticlesRepository {
  final ApiDataProvider dataProvider;

  ArticlesRepository(this.dataProvider);

  Future<PopularArticlesReponse> fetchMostViewedArticles(Period period) async {
    final res =
        await dataProvider.get("svc/mostpopular/v2/viewed/${period.days}.json");
    final json = jsonDecode(res.body);
    final articles = PopularArticlesReponse.fromJson(json);
    return articles;
  }

  Future<PopularArticlesReponse> fetchMostEmailedArticles(Period period) async {
    final res = await dataProvider
        .get("svc/mostpopular/v2/emailed/${period.days}.json");
    final json = jsonDecode(res.body);
    final articles = PopularArticlesReponse.fromJson(json);
    return articles;
  }

  Future<PopularArticlesReponse> fetchMostSharedArticles(Period period,
      {ShareType? shareType}) async {
    final shareTypeParam = shareType == null ? "" : "/${shareType.value}";
    final res = await dataProvider
        .get("svc/mostpopular/v2/shared/${period.days}$shareTypeParam.json");
    final json = jsonDecode(res.body);
    final articles = PopularArticlesReponse.fromJson(json);
    return articles;
  }

  Future<ArticlesReponse> searchArticles({
    String? beginDate,
    String? endDate,
    bool? facet,
    FacetFields? facetFields,
    bool? facetFilter,
    String? fieldList,
    String? filterQuery,
    int? page,
    String? query,
    SortMethod? sort,
  }) async {
    Map<String, dynamic> params = {};

    if (beginDate != null) params["begin_date"] = beginDate;
    if (endDate != null) params["end_date"] = endDate;
    if (facet != null) params["facet"] = facet.toString();
    if (facetFields != null) params["facet_fields"] = facetFields.value;
    if (facetFilter != null) params["facet_filter"] = facetFilter.toString();
    if (fieldList != null) params["fl"] = fieldList;
    if (filterQuery != null) params["fq"] = filterQuery;
    if (page != null) params["page"] = page;
    if (query != null) params["q"] = query;
    if (sort != null) params["sort"] = sort.value;

    final res = await dataProvider.getWithParams(
        "svc/search/v2/articlesearch.json", params);
    final json = jsonDecode(res.body);
    final result = ArticlesReponse.fromJson(json);
    return result;
  }
}
