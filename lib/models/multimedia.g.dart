// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'multimedia.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Multimedia _$_$_MultimediaFromJson(Map<String, dynamic> json) {
  return _$_Multimedia(
    json['rank'] as int,
    json['subtype'] as String? ?? '',
    json['caption'] as String? ?? '',
    json['credit'] as String? ?? '',
    json['type'] as String? ?? '',
    json['url'] as String? ?? '',
    json['height'] as int? ?? 0,
    json['width'] as int? ?? 0,
    json['crop_name'] as String? ?? '',
  );
}

Map<String, dynamic> _$_$_MultimediaToJson(_$_Multimedia instance) =>
    <String, dynamic>{
      'rank': instance.rank,
      'subtype': instance.subtype,
      'caption': instance.caption,
      'credit': instance.credit,
      'type': instance.type,
      'url': instance.url,
      'height': instance.height,
      'width': instance.width,
      'crop_name': instance.cropName,
    };
