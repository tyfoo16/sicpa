import 'package:sicpa/models/popular_article.dart';

class PopularArticlesReponse {
  final int resultsLength;
  final List<PopularArticle> results;

  PopularArticlesReponse(this.resultsLength, this.results);
  PopularArticlesReponse.fromJson(Map<String, dynamic> json)
      : resultsLength = json["num_results"] as int,
        results = (json["results"] as List<dynamic>)
            .map((e) => PopularArticle.fromJson(e))
            .toList();
}
