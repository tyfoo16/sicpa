import 'package:freezed_annotation/freezed_annotation.dart';

part 'media_metadata.freezed.dart';
part 'media_metadata.g.dart';

@freezed
class MediaMetadata with _$MediaMetadata {
  factory MediaMetadata(
    String url,
    String format,
    int height,
    int width,
  ) = _MediaMetadata;

  factory MediaMetadata.fromJson(Map<String, dynamic> json) =>
      _$MediaMetadataFromJson(json);
}
