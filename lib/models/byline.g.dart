// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'byline.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Byline _$_$_BylineFromJson(Map<String, dynamic> json) {
  return _$_Byline(
    json['original'] as String? ?? '',
    json['organization'] as String? ?? '',
    (json['person'] as List<dynamic>?)
            ?.map((e) => Person.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
  );
}

Map<String, dynamic> _$_$_BylineToJson(_$_Byline instance) => <String, dynamic>{
      'original': instance.original,
      'organization': instance.organization,
      'person': instance.person,
    };
