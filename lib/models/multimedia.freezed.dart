// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'multimedia.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Multimedia _$MultimediaFromJson(Map<String, dynamic> json) {
  return _Multimedia.fromJson(json);
}

/// @nodoc
class _$MultimediaTearOff {
  const _$MultimediaTearOff();

  _Multimedia call(
      int rank,
      @JsonKey(defaultValue: "") String? subtype,
      @JsonKey(defaultValue: "") String? caption,
      @JsonKey(defaultValue: "") String? credit,
      @JsonKey(defaultValue: "") String? type,
      @JsonKey(defaultValue: "") String? url,
      @JsonKey(defaultValue: 0) int? height,
      @JsonKey(defaultValue: 0) int? width,
      @JsonKey(defaultValue: "", name: "crop_name") String? cropName) {
    return _Multimedia(
      rank,
      subtype,
      caption,
      credit,
      type,
      url,
      height,
      width,
      cropName,
    );
  }

  Multimedia fromJson(Map<String, Object> json) {
    return Multimedia.fromJson(json);
  }
}

/// @nodoc
const $Multimedia = _$MultimediaTearOff();

/// @nodoc
mixin _$Multimedia {
  int get rank => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get subtype => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get caption => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get credit => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get type => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get url => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0)
  int? get height => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0)
  int? get width =>
      throw _privateConstructorUsedError; // @JsonKey(defaultValue: {}, name: "legacy") Map<String, dynamic> legacy,
  @JsonKey(defaultValue: "", name: "crop_name")
  String? get cropName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MultimediaCopyWith<Multimedia> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MultimediaCopyWith<$Res> {
  factory $MultimediaCopyWith(
          Multimedia value, $Res Function(Multimedia) then) =
      _$MultimediaCopyWithImpl<$Res>;
  $Res call(
      {int rank,
      @JsonKey(defaultValue: "") String? subtype,
      @JsonKey(defaultValue: "") String? caption,
      @JsonKey(defaultValue: "") String? credit,
      @JsonKey(defaultValue: "") String? type,
      @JsonKey(defaultValue: "") String? url,
      @JsonKey(defaultValue: 0) int? height,
      @JsonKey(defaultValue: 0) int? width,
      @JsonKey(defaultValue: "", name: "crop_name") String? cropName});
}

/// @nodoc
class _$MultimediaCopyWithImpl<$Res> implements $MultimediaCopyWith<$Res> {
  _$MultimediaCopyWithImpl(this._value, this._then);

  final Multimedia _value;
  // ignore: unused_field
  final $Res Function(Multimedia) _then;

  @override
  $Res call({
    Object? rank = freezed,
    Object? subtype = freezed,
    Object? caption = freezed,
    Object? credit = freezed,
    Object? type = freezed,
    Object? url = freezed,
    Object? height = freezed,
    Object? width = freezed,
    Object? cropName = freezed,
  }) {
    return _then(_value.copyWith(
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
      subtype: subtype == freezed
          ? _value.subtype
          : subtype // ignore: cast_nullable_to_non_nullable
              as String?,
      caption: caption == freezed
          ? _value.caption
          : caption // ignore: cast_nullable_to_non_nullable
              as String?,
      credit: credit == freezed
          ? _value.credit
          : credit // ignore: cast_nullable_to_non_nullable
              as String?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      height: height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int?,
      width: width == freezed
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int?,
      cropName: cropName == freezed
          ? _value.cropName
          : cropName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$MultimediaCopyWith<$Res> implements $MultimediaCopyWith<$Res> {
  factory _$MultimediaCopyWith(
          _Multimedia value, $Res Function(_Multimedia) then) =
      __$MultimediaCopyWithImpl<$Res>;
  @override
  $Res call(
      {int rank,
      @JsonKey(defaultValue: "") String? subtype,
      @JsonKey(defaultValue: "") String? caption,
      @JsonKey(defaultValue: "") String? credit,
      @JsonKey(defaultValue: "") String? type,
      @JsonKey(defaultValue: "") String? url,
      @JsonKey(defaultValue: 0) int? height,
      @JsonKey(defaultValue: 0) int? width,
      @JsonKey(defaultValue: "", name: "crop_name") String? cropName});
}

/// @nodoc
class __$MultimediaCopyWithImpl<$Res> extends _$MultimediaCopyWithImpl<$Res>
    implements _$MultimediaCopyWith<$Res> {
  __$MultimediaCopyWithImpl(
      _Multimedia _value, $Res Function(_Multimedia) _then)
      : super(_value, (v) => _then(v as _Multimedia));

  @override
  _Multimedia get _value => super._value as _Multimedia;

  @override
  $Res call({
    Object? rank = freezed,
    Object? subtype = freezed,
    Object? caption = freezed,
    Object? credit = freezed,
    Object? type = freezed,
    Object? url = freezed,
    Object? height = freezed,
    Object? width = freezed,
    Object? cropName = freezed,
  }) {
    return _then(_Multimedia(
      rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
      subtype == freezed
          ? _value.subtype
          : subtype // ignore: cast_nullable_to_non_nullable
              as String?,
      caption == freezed
          ? _value.caption
          : caption // ignore: cast_nullable_to_non_nullable
              as String?,
      credit == freezed
          ? _value.credit
          : credit // ignore: cast_nullable_to_non_nullable
              as String?,
      type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int?,
      width == freezed
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int?,
      cropName == freezed
          ? _value.cropName
          : cropName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Multimedia implements _Multimedia {
  _$_Multimedia(
      this.rank,
      @JsonKey(defaultValue: "") this.subtype,
      @JsonKey(defaultValue: "") this.caption,
      @JsonKey(defaultValue: "") this.credit,
      @JsonKey(defaultValue: "") this.type,
      @JsonKey(defaultValue: "") this.url,
      @JsonKey(defaultValue: 0) this.height,
      @JsonKey(defaultValue: 0) this.width,
      @JsonKey(defaultValue: "", name: "crop_name") this.cropName);

  factory _$_Multimedia.fromJson(Map<String, dynamic> json) =>
      _$_$_MultimediaFromJson(json);

  @override
  final int rank;
  @override
  @JsonKey(defaultValue: "")
  final String? subtype;
  @override
  @JsonKey(defaultValue: "")
  final String? caption;
  @override
  @JsonKey(defaultValue: "")
  final String? credit;
  @override
  @JsonKey(defaultValue: "")
  final String? type;
  @override
  @JsonKey(defaultValue: "")
  final String? url;
  @override
  @JsonKey(defaultValue: 0)
  final int? height;
  @override
  @JsonKey(defaultValue: 0)
  final int? width;
  @override // @JsonKey(defaultValue: {}, name: "legacy") Map<String, dynamic> legacy,
  @JsonKey(defaultValue: "", name: "crop_name")
  final String? cropName;

  @override
  String toString() {
    return 'Multimedia(rank: $rank, subtype: $subtype, caption: $caption, credit: $credit, type: $type, url: $url, height: $height, width: $width, cropName: $cropName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Multimedia &&
            (identical(other.rank, rank) ||
                const DeepCollectionEquality().equals(other.rank, rank)) &&
            (identical(other.subtype, subtype) ||
                const DeepCollectionEquality()
                    .equals(other.subtype, subtype)) &&
            (identical(other.caption, caption) ||
                const DeepCollectionEquality()
                    .equals(other.caption, caption)) &&
            (identical(other.credit, credit) ||
                const DeepCollectionEquality().equals(other.credit, credit)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.height, height) ||
                const DeepCollectionEquality().equals(other.height, height)) &&
            (identical(other.width, width) ||
                const DeepCollectionEquality().equals(other.width, width)) &&
            (identical(other.cropName, cropName) ||
                const DeepCollectionEquality()
                    .equals(other.cropName, cropName)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(rank) ^
      const DeepCollectionEquality().hash(subtype) ^
      const DeepCollectionEquality().hash(caption) ^
      const DeepCollectionEquality().hash(credit) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(height) ^
      const DeepCollectionEquality().hash(width) ^
      const DeepCollectionEquality().hash(cropName);

  @JsonKey(ignore: true)
  @override
  _$MultimediaCopyWith<_Multimedia> get copyWith =>
      __$MultimediaCopyWithImpl<_Multimedia>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MultimediaToJson(this);
  }
}

abstract class _Multimedia implements Multimedia {
  factory _Multimedia(
          int rank,
          @JsonKey(defaultValue: "") String? subtype,
          @JsonKey(defaultValue: "") String? caption,
          @JsonKey(defaultValue: "") String? credit,
          @JsonKey(defaultValue: "") String? type,
          @JsonKey(defaultValue: "") String? url,
          @JsonKey(defaultValue: 0) int? height,
          @JsonKey(defaultValue: 0) int? width,
          @JsonKey(defaultValue: "", name: "crop_name") String? cropName) =
      _$_Multimedia;

  factory _Multimedia.fromJson(Map<String, dynamic> json) =
      _$_Multimedia.fromJson;

  @override
  int get rank => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get subtype => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get caption => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get credit => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get type => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get url => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: 0)
  int? get height => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: 0)
  int? get width => throw _privateConstructorUsedError;
  @override // @JsonKey(defaultValue: {}, name: "legacy") Map<String, dynamic> legacy,
  @JsonKey(defaultValue: "", name: "crop_name")
  String? get cropName => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MultimediaCopyWith<_Multimedia> get copyWith =>
      throw _privateConstructorUsedError;
}
