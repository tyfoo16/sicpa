// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'byline.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Byline _$BylineFromJson(Map<String, dynamic> json) {
  return _Byline.fromJson(json);
}

/// @nodoc
class _$BylineTearOff {
  const _$BylineTearOff();

  _Byline call(
      @JsonKey(defaultValue: "") String? original,
      @JsonKey(defaultValue: "") String? organization,
      @JsonKey(defaultValue: const []) List<Person> person) {
    return _Byline(
      original,
      organization,
      person,
    );
  }

  Byline fromJson(Map<String, Object> json) {
    return Byline.fromJson(json);
  }
}

/// @nodoc
const $Byline = _$BylineTearOff();

/// @nodoc
mixin _$Byline {
  @JsonKey(defaultValue: "")
  String? get original => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get organization => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: const [])
  List<Person> get person => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BylineCopyWith<Byline> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BylineCopyWith<$Res> {
  factory $BylineCopyWith(Byline value, $Res Function(Byline) then) =
      _$BylineCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: "") String? original,
      @JsonKey(defaultValue: "") String? organization,
      @JsonKey(defaultValue: const []) List<Person> person});
}

/// @nodoc
class _$BylineCopyWithImpl<$Res> implements $BylineCopyWith<$Res> {
  _$BylineCopyWithImpl(this._value, this._then);

  final Byline _value;
  // ignore: unused_field
  final $Res Function(Byline) _then;

  @override
  $Res call({
    Object? original = freezed,
    Object? organization = freezed,
    Object? person = freezed,
  }) {
    return _then(_value.copyWith(
      original: original == freezed
          ? _value.original
          : original // ignore: cast_nullable_to_non_nullable
              as String?,
      organization: organization == freezed
          ? _value.organization
          : organization // ignore: cast_nullable_to_non_nullable
              as String?,
      person: person == freezed
          ? _value.person
          : person // ignore: cast_nullable_to_non_nullable
              as List<Person>,
    ));
  }
}

/// @nodoc
abstract class _$BylineCopyWith<$Res> implements $BylineCopyWith<$Res> {
  factory _$BylineCopyWith(_Byline value, $Res Function(_Byline) then) =
      __$BylineCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: "") String? original,
      @JsonKey(defaultValue: "") String? organization,
      @JsonKey(defaultValue: const []) List<Person> person});
}

/// @nodoc
class __$BylineCopyWithImpl<$Res> extends _$BylineCopyWithImpl<$Res>
    implements _$BylineCopyWith<$Res> {
  __$BylineCopyWithImpl(_Byline _value, $Res Function(_Byline) _then)
      : super(_value, (v) => _then(v as _Byline));

  @override
  _Byline get _value => super._value as _Byline;

  @override
  $Res call({
    Object? original = freezed,
    Object? organization = freezed,
    Object? person = freezed,
  }) {
    return _then(_Byline(
      original == freezed
          ? _value.original
          : original // ignore: cast_nullable_to_non_nullable
              as String?,
      organization == freezed
          ? _value.organization
          : organization // ignore: cast_nullable_to_non_nullable
              as String?,
      person == freezed
          ? _value.person
          : person // ignore: cast_nullable_to_non_nullable
              as List<Person>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Byline implements _Byline {
  _$_Byline(
      @JsonKey(defaultValue: "") this.original,
      @JsonKey(defaultValue: "") this.organization,
      @JsonKey(defaultValue: const []) this.person);

  factory _$_Byline.fromJson(Map<String, dynamic> json) =>
      _$_$_BylineFromJson(json);

  @override
  @JsonKey(defaultValue: "")
  final String? original;
  @override
  @JsonKey(defaultValue: "")
  final String? organization;
  @override
  @JsonKey(defaultValue: const [])
  final List<Person> person;

  @override
  String toString() {
    return 'Byline(original: $original, organization: $organization, person: $person)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Byline &&
            (identical(other.original, original) ||
                const DeepCollectionEquality()
                    .equals(other.original, original)) &&
            (identical(other.organization, organization) ||
                const DeepCollectionEquality()
                    .equals(other.organization, organization)) &&
            (identical(other.person, person) ||
                const DeepCollectionEquality().equals(other.person, person)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(original) ^
      const DeepCollectionEquality().hash(organization) ^
      const DeepCollectionEquality().hash(person);

  @JsonKey(ignore: true)
  @override
  _$BylineCopyWith<_Byline> get copyWith =>
      __$BylineCopyWithImpl<_Byline>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_BylineToJson(this);
  }
}

abstract class _Byline implements Byline {
  factory _Byline(
      @JsonKey(defaultValue: "") String? original,
      @JsonKey(defaultValue: "") String? organization,
      @JsonKey(defaultValue: const []) List<Person> person) = _$_Byline;

  factory _Byline.fromJson(Map<String, dynamic> json) = _$_Byline.fromJson;

  @override
  @JsonKey(defaultValue: "")
  String? get original => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get organization => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: const [])
  List<Person> get person => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$BylineCopyWith<_Byline> get copyWith => throw _privateConstructorUsedError;
}
