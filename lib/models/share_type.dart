enum ShareType {
  Facebook,
}

extension ShareTypeExtension on ShareType {
  String get value {
    switch (this) {
      case ShareType.Facebook:
        return "facebook";
      default:
        return "facebook";
    }
  }
}
