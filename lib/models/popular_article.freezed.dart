// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'popular_article.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PopularArticle _$PopularArticleFromJson(Map<String, dynamic> json) {
  return _PopularArticle.fromJson(json);
}

/// @nodoc
class _$PopularArticleTearOff {
  const _$PopularArticleTearOff();

  _PopularArticle call(
      String uri,
      String url,
      int id,
      @JsonKey(name: 'asset_id') int assetId,
      String source,
      @JsonKey(name: 'published_date') String publishedDate,
      String updated,
      String section,
      String subsection,
      String nytdsection,
      @JsonKey(name: 'adx_keywords') String adxKeywords,
      @deprecated String? column,
      String byline,
      String type,
      String title,
      @JsonKey(name: 'abstract') String abstractStr,
      @JsonKey(name: 'des_facet') List<String> desFacet,
      @JsonKey(name: 'org_facet') List<String> orgFacet,
      @JsonKey(name: 'per_facet') List<String> perFacet,
      @JsonKey(name: 'geo_facet') List<String> geoFacet,
      List<Media> media,
      @deprecated @JsonKey(name: 'eta_id') int etaId) {
    return _PopularArticle(
      uri,
      url,
      id,
      assetId,
      source,
      publishedDate,
      updated,
      section,
      subsection,
      nytdsection,
      adxKeywords,
      column,
      byline,
      type,
      title,
      abstractStr,
      desFacet,
      orgFacet,
      perFacet,
      geoFacet,
      media,
      etaId,
    );
  }

  PopularArticle fromJson(Map<String, Object> json) {
    return PopularArticle.fromJson(json);
  }
}

/// @nodoc
const $PopularArticle = _$PopularArticleTearOff();

/// @nodoc
mixin _$PopularArticle {
  String get uri => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'asset_id')
  int get assetId => throw _privateConstructorUsedError;
  String get source => throw _privateConstructorUsedError;
  @JsonKey(name: 'published_date')
  String get publishedDate => throw _privateConstructorUsedError;
  String get updated => throw _privateConstructorUsedError;
  String get section => throw _privateConstructorUsedError;
  String get subsection => throw _privateConstructorUsedError;
  String get nytdsection => throw _privateConstructorUsedError;
  @JsonKey(name: 'adx_keywords')
  String get adxKeywords => throw _privateConstructorUsedError;
  @deprecated
  String? get column => throw _privateConstructorUsedError;
  String get byline => throw _privateConstructorUsedError;
  String get type => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  @JsonKey(name: 'abstract')
  String get abstractStr => throw _privateConstructorUsedError;
  @JsonKey(name: 'des_facet')
  List<String> get desFacet => throw _privateConstructorUsedError;
  @JsonKey(name: 'org_facet')
  List<String> get orgFacet => throw _privateConstructorUsedError;
  @JsonKey(name: 'per_facet')
  List<String> get perFacet => throw _privateConstructorUsedError;
  @JsonKey(name: 'geo_facet')
  List<String> get geoFacet => throw _privateConstructorUsedError;
  List<Media> get media => throw _privateConstructorUsedError;
  @deprecated
  @JsonKey(name: 'eta_id')
  int get etaId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PopularArticleCopyWith<PopularArticle> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PopularArticleCopyWith<$Res> {
  factory $PopularArticleCopyWith(
          PopularArticle value, $Res Function(PopularArticle) then) =
      _$PopularArticleCopyWithImpl<$Res>;
  $Res call(
      {String uri,
      String url,
      int id,
      @JsonKey(name: 'asset_id') int assetId,
      String source,
      @JsonKey(name: 'published_date') String publishedDate,
      String updated,
      String section,
      String subsection,
      String nytdsection,
      @JsonKey(name: 'adx_keywords') String adxKeywords,
      @deprecated String? column,
      String byline,
      String type,
      String title,
      @JsonKey(name: 'abstract') String abstractStr,
      @JsonKey(name: 'des_facet') List<String> desFacet,
      @JsonKey(name: 'org_facet') List<String> orgFacet,
      @JsonKey(name: 'per_facet') List<String> perFacet,
      @JsonKey(name: 'geo_facet') List<String> geoFacet,
      List<Media> media,
      @deprecated @JsonKey(name: 'eta_id') int etaId});
}

/// @nodoc
class _$PopularArticleCopyWithImpl<$Res>
    implements $PopularArticleCopyWith<$Res> {
  _$PopularArticleCopyWithImpl(this._value, this._then);

  final PopularArticle _value;
  // ignore: unused_field
  final $Res Function(PopularArticle) _then;

  @override
  $Res call({
    Object? uri = freezed,
    Object? url = freezed,
    Object? id = freezed,
    Object? assetId = freezed,
    Object? source = freezed,
    Object? publishedDate = freezed,
    Object? updated = freezed,
    Object? section = freezed,
    Object? subsection = freezed,
    Object? nytdsection = freezed,
    Object? adxKeywords = freezed,
    Object? column = freezed,
    Object? byline = freezed,
    Object? type = freezed,
    Object? title = freezed,
    Object? abstractStr = freezed,
    Object? desFacet = freezed,
    Object? orgFacet = freezed,
    Object? perFacet = freezed,
    Object? geoFacet = freezed,
    Object? media = freezed,
    Object? etaId = freezed,
  }) {
    return _then(_value.copyWith(
      uri: uri == freezed
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      assetId: assetId == freezed
          ? _value.assetId
          : assetId // ignore: cast_nullable_to_non_nullable
              as int,
      source: source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String,
      publishedDate: publishedDate == freezed
          ? _value.publishedDate
          : publishedDate // ignore: cast_nullable_to_non_nullable
              as String,
      updated: updated == freezed
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String,
      section: section == freezed
          ? _value.section
          : section // ignore: cast_nullable_to_non_nullable
              as String,
      subsection: subsection == freezed
          ? _value.subsection
          : subsection // ignore: cast_nullable_to_non_nullable
              as String,
      nytdsection: nytdsection == freezed
          ? _value.nytdsection
          : nytdsection // ignore: cast_nullable_to_non_nullable
              as String,
      adxKeywords: adxKeywords == freezed
          ? _value.adxKeywords
          : adxKeywords // ignore: cast_nullable_to_non_nullable
              as String,
      column: column == freezed
          ? _value.column
          : column // ignore: cast_nullable_to_non_nullable
              as String?,
      byline: byline == freezed
          ? _value.byline
          : byline // ignore: cast_nullable_to_non_nullable
              as String,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      abstractStr: abstractStr == freezed
          ? _value.abstractStr
          : abstractStr // ignore: cast_nullable_to_non_nullable
              as String,
      desFacet: desFacet == freezed
          ? _value.desFacet
          : desFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      orgFacet: orgFacet == freezed
          ? _value.orgFacet
          : orgFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      perFacet: perFacet == freezed
          ? _value.perFacet
          : perFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      geoFacet: geoFacet == freezed
          ? _value.geoFacet
          : geoFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      media: media == freezed
          ? _value.media
          : media // ignore: cast_nullable_to_non_nullable
              as List<Media>,
      etaId: etaId == freezed
          ? _value.etaId
          : etaId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$PopularArticleCopyWith<$Res>
    implements $PopularArticleCopyWith<$Res> {
  factory _$PopularArticleCopyWith(
          _PopularArticle value, $Res Function(_PopularArticle) then) =
      __$PopularArticleCopyWithImpl<$Res>;
  @override
  $Res call(
      {String uri,
      String url,
      int id,
      @JsonKey(name: 'asset_id') int assetId,
      String source,
      @JsonKey(name: 'published_date') String publishedDate,
      String updated,
      String section,
      String subsection,
      String nytdsection,
      @JsonKey(name: 'adx_keywords') String adxKeywords,
      @deprecated String? column,
      String byline,
      String type,
      String title,
      @JsonKey(name: 'abstract') String abstractStr,
      @JsonKey(name: 'des_facet') List<String> desFacet,
      @JsonKey(name: 'org_facet') List<String> orgFacet,
      @JsonKey(name: 'per_facet') List<String> perFacet,
      @JsonKey(name: 'geo_facet') List<String> geoFacet,
      List<Media> media,
      @deprecated @JsonKey(name: 'eta_id') int etaId});
}

/// @nodoc
class __$PopularArticleCopyWithImpl<$Res>
    extends _$PopularArticleCopyWithImpl<$Res>
    implements _$PopularArticleCopyWith<$Res> {
  __$PopularArticleCopyWithImpl(
      _PopularArticle _value, $Res Function(_PopularArticle) _then)
      : super(_value, (v) => _then(v as _PopularArticle));

  @override
  _PopularArticle get _value => super._value as _PopularArticle;

  @override
  $Res call({
    Object? uri = freezed,
    Object? url = freezed,
    Object? id = freezed,
    Object? assetId = freezed,
    Object? source = freezed,
    Object? publishedDate = freezed,
    Object? updated = freezed,
    Object? section = freezed,
    Object? subsection = freezed,
    Object? nytdsection = freezed,
    Object? adxKeywords = freezed,
    Object? column = freezed,
    Object? byline = freezed,
    Object? type = freezed,
    Object? title = freezed,
    Object? abstractStr = freezed,
    Object? desFacet = freezed,
    Object? orgFacet = freezed,
    Object? perFacet = freezed,
    Object? geoFacet = freezed,
    Object? media = freezed,
    Object? etaId = freezed,
  }) {
    return _then(_PopularArticle(
      uri == freezed
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as String,
      url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      assetId == freezed
          ? _value.assetId
          : assetId // ignore: cast_nullable_to_non_nullable
              as int,
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String,
      publishedDate == freezed
          ? _value.publishedDate
          : publishedDate // ignore: cast_nullable_to_non_nullable
              as String,
      updated == freezed
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String,
      section == freezed
          ? _value.section
          : section // ignore: cast_nullable_to_non_nullable
              as String,
      subsection == freezed
          ? _value.subsection
          : subsection // ignore: cast_nullable_to_non_nullable
              as String,
      nytdsection == freezed
          ? _value.nytdsection
          : nytdsection // ignore: cast_nullable_to_non_nullable
              as String,
      adxKeywords == freezed
          ? _value.adxKeywords
          : adxKeywords // ignore: cast_nullable_to_non_nullable
              as String,
      column == freezed
          ? _value.column
          : column // ignore: cast_nullable_to_non_nullable
              as String?,
      byline == freezed
          ? _value.byline
          : byline // ignore: cast_nullable_to_non_nullable
              as String,
      type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      abstractStr == freezed
          ? _value.abstractStr
          : abstractStr // ignore: cast_nullable_to_non_nullable
              as String,
      desFacet == freezed
          ? _value.desFacet
          : desFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      orgFacet == freezed
          ? _value.orgFacet
          : orgFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      perFacet == freezed
          ? _value.perFacet
          : perFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      geoFacet == freezed
          ? _value.geoFacet
          : geoFacet // ignore: cast_nullable_to_non_nullable
              as List<String>,
      media == freezed
          ? _value.media
          : media // ignore: cast_nullable_to_non_nullable
              as List<Media>,
      etaId == freezed
          ? _value.etaId
          : etaId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PopularArticle implements _PopularArticle {
  _$_PopularArticle(
      this.uri,
      this.url,
      this.id,
      @JsonKey(name: 'asset_id') this.assetId,
      this.source,
      @JsonKey(name: 'published_date') this.publishedDate,
      this.updated,
      this.section,
      this.subsection,
      this.nytdsection,
      @JsonKey(name: 'adx_keywords') this.adxKeywords,
      @deprecated this.column,
      this.byline,
      this.type,
      this.title,
      @JsonKey(name: 'abstract') this.abstractStr,
      @JsonKey(name: 'des_facet') this.desFacet,
      @JsonKey(name: 'org_facet') this.orgFacet,
      @JsonKey(name: 'per_facet') this.perFacet,
      @JsonKey(name: 'geo_facet') this.geoFacet,
      this.media,
      @deprecated @JsonKey(name: 'eta_id') this.etaId);

  factory _$_PopularArticle.fromJson(Map<String, dynamic> json) =>
      _$_$_PopularArticleFromJson(json);

  @override
  final String uri;
  @override
  final String url;
  @override
  final int id;
  @override
  @JsonKey(name: 'asset_id')
  final int assetId;
  @override
  final String source;
  @override
  @JsonKey(name: 'published_date')
  final String publishedDate;
  @override
  final String updated;
  @override
  final String section;
  @override
  final String subsection;
  @override
  final String nytdsection;
  @override
  @JsonKey(name: 'adx_keywords')
  final String adxKeywords;
  @override
  @deprecated
  final String? column;
  @override
  final String byline;
  @override
  final String type;
  @override
  final String title;
  @override
  @JsonKey(name: 'abstract')
  final String abstractStr;
  @override
  @JsonKey(name: 'des_facet')
  final List<String> desFacet;
  @override
  @JsonKey(name: 'org_facet')
  final List<String> orgFacet;
  @override
  @JsonKey(name: 'per_facet')
  final List<String> perFacet;
  @override
  @JsonKey(name: 'geo_facet')
  final List<String> geoFacet;
  @override
  final List<Media> media;
  @override
  @deprecated
  @JsonKey(name: 'eta_id')
  final int etaId;

  @override
  String toString() {
    return 'PopularArticle(uri: $uri, url: $url, id: $id, assetId: $assetId, source: $source, publishedDate: $publishedDate, updated: $updated, section: $section, subsection: $subsection, nytdsection: $nytdsection, adxKeywords: $adxKeywords, column: $column, byline: $byline, type: $type, title: $title, abstractStr: $abstractStr, desFacet: $desFacet, orgFacet: $orgFacet, perFacet: $perFacet, geoFacet: $geoFacet, media: $media, etaId: $etaId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PopularArticle &&
            (identical(other.uri, uri) ||
                const DeepCollectionEquality().equals(other.uri, uri)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.assetId, assetId) ||
                const DeepCollectionEquality()
                    .equals(other.assetId, assetId)) &&
            (identical(other.source, source) ||
                const DeepCollectionEquality().equals(other.source, source)) &&
            (identical(other.publishedDate, publishedDate) ||
                const DeepCollectionEquality()
                    .equals(other.publishedDate, publishedDate)) &&
            (identical(other.updated, updated) ||
                const DeepCollectionEquality()
                    .equals(other.updated, updated)) &&
            (identical(other.section, section) ||
                const DeepCollectionEquality()
                    .equals(other.section, section)) &&
            (identical(other.subsection, subsection) ||
                const DeepCollectionEquality()
                    .equals(other.subsection, subsection)) &&
            (identical(other.nytdsection, nytdsection) ||
                const DeepCollectionEquality()
                    .equals(other.nytdsection, nytdsection)) &&
            (identical(other.adxKeywords, adxKeywords) ||
                const DeepCollectionEquality()
                    .equals(other.adxKeywords, adxKeywords)) &&
            (identical(other.column, column) ||
                const DeepCollectionEquality().equals(other.column, column)) &&
            (identical(other.byline, byline) ||
                const DeepCollectionEquality().equals(other.byline, byline)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.abstractStr, abstractStr) ||
                const DeepCollectionEquality()
                    .equals(other.abstractStr, abstractStr)) &&
            (identical(other.desFacet, desFacet) ||
                const DeepCollectionEquality()
                    .equals(other.desFacet, desFacet)) &&
            (identical(other.orgFacet, orgFacet) ||
                const DeepCollectionEquality()
                    .equals(other.orgFacet, orgFacet)) &&
            (identical(other.perFacet, perFacet) ||
                const DeepCollectionEquality()
                    .equals(other.perFacet, perFacet)) &&
            (identical(other.geoFacet, geoFacet) ||
                const DeepCollectionEquality()
                    .equals(other.geoFacet, geoFacet)) &&
            (identical(other.media, media) ||
                const DeepCollectionEquality().equals(other.media, media)) &&
            (identical(other.etaId, etaId) ||
                const DeepCollectionEquality().equals(other.etaId, etaId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(uri) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(assetId) ^
      const DeepCollectionEquality().hash(source) ^
      const DeepCollectionEquality().hash(publishedDate) ^
      const DeepCollectionEquality().hash(updated) ^
      const DeepCollectionEquality().hash(section) ^
      const DeepCollectionEquality().hash(subsection) ^
      const DeepCollectionEquality().hash(nytdsection) ^
      const DeepCollectionEquality().hash(adxKeywords) ^
      const DeepCollectionEquality().hash(column) ^
      const DeepCollectionEquality().hash(byline) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(abstractStr) ^
      const DeepCollectionEquality().hash(desFacet) ^
      const DeepCollectionEquality().hash(orgFacet) ^
      const DeepCollectionEquality().hash(perFacet) ^
      const DeepCollectionEquality().hash(geoFacet) ^
      const DeepCollectionEquality().hash(media) ^
      const DeepCollectionEquality().hash(etaId);

  @JsonKey(ignore: true)
  @override
  _$PopularArticleCopyWith<_PopularArticle> get copyWith =>
      __$PopularArticleCopyWithImpl<_PopularArticle>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PopularArticleToJson(this);
  }
}

abstract class _PopularArticle implements PopularArticle {
  factory _PopularArticle(
      String uri,
      String url,
      int id,
      @JsonKey(name: 'asset_id') int assetId,
      String source,
      @JsonKey(name: 'published_date') String publishedDate,
      String updated,
      String section,
      String subsection,
      String nytdsection,
      @JsonKey(name: 'adx_keywords') String adxKeywords,
      @deprecated String? column,
      String byline,
      String type,
      String title,
      @JsonKey(name: 'abstract') String abstractStr,
      @JsonKey(name: 'des_facet') List<String> desFacet,
      @JsonKey(name: 'org_facet') List<String> orgFacet,
      @JsonKey(name: 'per_facet') List<String> perFacet,
      @JsonKey(name: 'geo_facet') List<String> geoFacet,
      List<Media> media,
      @deprecated @JsonKey(name: 'eta_id') int etaId) = _$_PopularArticle;

  factory _PopularArticle.fromJson(Map<String, dynamic> json) =
      _$_PopularArticle.fromJson;

  @override
  String get uri => throw _privateConstructorUsedError;
  @override
  String get url => throw _privateConstructorUsedError;
  @override
  int get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'asset_id')
  int get assetId => throw _privateConstructorUsedError;
  @override
  String get source => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'published_date')
  String get publishedDate => throw _privateConstructorUsedError;
  @override
  String get updated => throw _privateConstructorUsedError;
  @override
  String get section => throw _privateConstructorUsedError;
  @override
  String get subsection => throw _privateConstructorUsedError;
  @override
  String get nytdsection => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'adx_keywords')
  String get adxKeywords => throw _privateConstructorUsedError;
  @override
  @deprecated
  String? get column => throw _privateConstructorUsedError;
  @override
  String get byline => throw _privateConstructorUsedError;
  @override
  String get type => throw _privateConstructorUsedError;
  @override
  String get title => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'abstract')
  String get abstractStr => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'des_facet')
  List<String> get desFacet => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'org_facet')
  List<String> get orgFacet => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'per_facet')
  List<String> get perFacet => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'geo_facet')
  List<String> get geoFacet => throw _privateConstructorUsedError;
  @override
  List<Media> get media => throw _privateConstructorUsedError;
  @override
  @deprecated
  @JsonKey(name: 'eta_id')
  int get etaId => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PopularArticleCopyWith<_PopularArticle> get copyWith =>
      throw _privateConstructorUsedError;
}
