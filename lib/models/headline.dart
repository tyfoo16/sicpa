// "main": "5 Minutes That Will Make You Love Classical Music",
// "kicker": null,
// "content_kicker": null,
// "print_headline": null,
// "name": null,
// "seo": null,
// "sub": null

import 'package:freezed_annotation/freezed_annotation.dart';

part 'headline.freezed.dart';
part 'headline.g.dart';

@freezed
class Headline with _$Headline {
  factory Headline(
    String main,
    @JsonKey(defaultValue: "") String? kicker,
    @JsonKey(defaultValue: "", name: "content_kicker") String? contentKicker,
    @JsonKey(defaultValue: "", name: "print_headline") String? printHeadline,
    @JsonKey(defaultValue: "") String? name,
    @JsonKey(defaultValue: "") String? seo,
    @JsonKey(defaultValue: "") String? sub,
  ) = _Headline;

  factory Headline.fromJson(Map<String, dynamic> json) =>
      _$HeadlineFromJson(json);
}
