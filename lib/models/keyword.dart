// {
//     "name": "subject",
//     "value": "Smartphones",
//     "rank": 1,
//     "major": "N"
// },

import 'package:freezed_annotation/freezed_annotation.dart';

part 'keyword.freezed.dart';
part 'keyword.g.dart';

@freezed
class Keyword with _$Keyword {
  factory Keyword(
    String name,
    String value,
    int rank,
    String major,
  ) = _Keyword;

  factory Keyword.fromJson(Map<String, dynamic> json) =>
      _$KeywordFromJson(json);
}
