//  "byline": {
//     "original": null,
//     "person": [],
//     "organization": null
// },

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/person.dart';

part 'byline.freezed.dart';
part 'byline.g.dart';

@freezed
class Byline with _$Byline {
  factory Byline(
    @JsonKey(defaultValue: "") String? original,
    @JsonKey(defaultValue: "") String? organization,
    @JsonKey(defaultValue: []) List<Person> person,
  ) = _Byline;

  factory Byline.fromJson(Map<String, dynamic> json) => _$BylineFromJson(json);
}
