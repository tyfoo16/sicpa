// type_of_material
// string

// _id
// string

// word_count
// integer

// score
// integer

// uri
// string

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/byline.dart';
import 'package:sicpa/models/headline.dart';
import 'package:sicpa/models/keyword.dart';
import 'package:sicpa/models/multimedia.dart';

part 'article.freezed.dart';
part 'article.g.dart';

@freezed
class Article with _$Article {
  factory Article(
    @JsonKey(defaultValue: "", name: "web_url") String? webUrl,
    @JsonKey(defaultValue: "") String? snippet,
    @JsonKey(defaultValue: "0", name: "print_page") String? printPage,
    @JsonKey(defaultValue: "") String? source,
    @JsonKey(defaultValue: []) List<Multimedia> multimedia,
    Headline headline,
    @JsonKey(defaultValue: []) List<Keyword> keywords,
    @JsonKey(defaultValue: "", name: "pub_date") String? pubDate,
    @JsonKey(defaultValue: "", name: "document_type") String? documentType,
    @JsonKey(defaultValue: "", name: "news_desk") String? newsDesk,
    Byline byline,
    @JsonKey(defaultValue: "", name: "type_of_material") String? typeOfMaterial,
    @JsonKey(defaultValue: "", name: "_id") String? id,
    @JsonKey(defaultValue: 0, name: "word_count") int? wordCount,
    @JsonKey(defaultValue: 0) int? score,
    @JsonKey(defaultValue: "") String uri,
  ) = _Article;

  factory Article.fromJson(Map<String, dynamic> json) =>
      _$ArticleFromJson(json);
}
