// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'media_metadata.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MediaMetadata _$MediaMetadataFromJson(Map<String, dynamic> json) {
  return _MediaMetadata.fromJson(json);
}

/// @nodoc
class _$MediaMetadataTearOff {
  const _$MediaMetadataTearOff();

  _MediaMetadata call(String url, String format, int height, int width) {
    return _MediaMetadata(
      url,
      format,
      height,
      width,
    );
  }

  MediaMetadata fromJson(Map<String, Object> json) {
    return MediaMetadata.fromJson(json);
  }
}

/// @nodoc
const $MediaMetadata = _$MediaMetadataTearOff();

/// @nodoc
mixin _$MediaMetadata {
  String get url => throw _privateConstructorUsedError;
  String get format => throw _privateConstructorUsedError;
  int get height => throw _privateConstructorUsedError;
  int get width => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MediaMetadataCopyWith<MediaMetadata> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MediaMetadataCopyWith<$Res> {
  factory $MediaMetadataCopyWith(
          MediaMetadata value, $Res Function(MediaMetadata) then) =
      _$MediaMetadataCopyWithImpl<$Res>;
  $Res call({String url, String format, int height, int width});
}

/// @nodoc
class _$MediaMetadataCopyWithImpl<$Res>
    implements $MediaMetadataCopyWith<$Res> {
  _$MediaMetadataCopyWithImpl(this._value, this._then);

  final MediaMetadata _value;
  // ignore: unused_field
  final $Res Function(MediaMetadata) _then;

  @override
  $Res call({
    Object? url = freezed,
    Object? format = freezed,
    Object? height = freezed,
    Object? width = freezed,
  }) {
    return _then(_value.copyWith(
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      format: format == freezed
          ? _value.format
          : format // ignore: cast_nullable_to_non_nullable
              as String,
      height: height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int,
      width: width == freezed
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$MediaMetadataCopyWith<$Res>
    implements $MediaMetadataCopyWith<$Res> {
  factory _$MediaMetadataCopyWith(
          _MediaMetadata value, $Res Function(_MediaMetadata) then) =
      __$MediaMetadataCopyWithImpl<$Res>;
  @override
  $Res call({String url, String format, int height, int width});
}

/// @nodoc
class __$MediaMetadataCopyWithImpl<$Res>
    extends _$MediaMetadataCopyWithImpl<$Res>
    implements _$MediaMetadataCopyWith<$Res> {
  __$MediaMetadataCopyWithImpl(
      _MediaMetadata _value, $Res Function(_MediaMetadata) _then)
      : super(_value, (v) => _then(v as _MediaMetadata));

  @override
  _MediaMetadata get _value => super._value as _MediaMetadata;

  @override
  $Res call({
    Object? url = freezed,
    Object? format = freezed,
    Object? height = freezed,
    Object? width = freezed,
  }) {
    return _then(_MediaMetadata(
      url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      format == freezed
          ? _value.format
          : format // ignore: cast_nullable_to_non_nullable
              as String,
      height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int,
      width == freezed
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MediaMetadata implements _MediaMetadata {
  _$_MediaMetadata(this.url, this.format, this.height, this.width);

  factory _$_MediaMetadata.fromJson(Map<String, dynamic> json) =>
      _$_$_MediaMetadataFromJson(json);

  @override
  final String url;
  @override
  final String format;
  @override
  final int height;
  @override
  final int width;

  @override
  String toString() {
    return 'MediaMetadata(url: $url, format: $format, height: $height, width: $width)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MediaMetadata &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.format, format) ||
                const DeepCollectionEquality().equals(other.format, format)) &&
            (identical(other.height, height) ||
                const DeepCollectionEquality().equals(other.height, height)) &&
            (identical(other.width, width) ||
                const DeepCollectionEquality().equals(other.width, width)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(format) ^
      const DeepCollectionEquality().hash(height) ^
      const DeepCollectionEquality().hash(width);

  @JsonKey(ignore: true)
  @override
  _$MediaMetadataCopyWith<_MediaMetadata> get copyWith =>
      __$MediaMetadataCopyWithImpl<_MediaMetadata>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MediaMetadataToJson(this);
  }
}

abstract class _MediaMetadata implements MediaMetadata {
  factory _MediaMetadata(String url, String format, int height, int width) =
      _$_MediaMetadata;

  factory _MediaMetadata.fromJson(Map<String, dynamic> json) =
      _$_MediaMetadata.fromJson;

  @override
  String get url => throw _privateConstructorUsedError;
  @override
  String get format => throw _privateConstructorUsedError;
  @override
  int get height => throw _privateConstructorUsedError;
  @override
  int get width => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MediaMetadataCopyWith<_MediaMetadata> get copyWith =>
      throw _privateConstructorUsedError;
}
