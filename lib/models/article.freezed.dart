// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'article.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Article _$ArticleFromJson(Map<String, dynamic> json) {
  return _Article.fromJson(json);
}

/// @nodoc
class _$ArticleTearOff {
  const _$ArticleTearOff();

  _Article call(
      @JsonKey(defaultValue: "", name: "web_url")
          String? webUrl,
      @JsonKey(defaultValue: "")
          String? snippet,
      @JsonKey(defaultValue: "0", name: "print_page")
          String? printPage,
      @JsonKey(defaultValue: "")
          String? source,
      @JsonKey(defaultValue: const [])
          List<Multimedia> multimedia,
      Headline headline,
      @JsonKey(defaultValue: const [])
          List<Keyword> keywords,
      @JsonKey(defaultValue: "", name: "pub_date")
          String? pubDate,
      @JsonKey(defaultValue: "", name: "document_type")
          String? documentType,
      @JsonKey(defaultValue: "", name: "news_desk")
          String? newsDesk,
      Byline byline,
      @JsonKey(defaultValue: "", name: "type_of_material")
          String? typeOfMaterial,
      @JsonKey(defaultValue: "", name: "_id")
          String? id,
      @JsonKey(defaultValue: 0, name: "word_count")
          int? wordCount,
      @JsonKey(defaultValue: 0)
          int? score,
      @JsonKey(defaultValue: "")
          String uri) {
    return _Article(
      webUrl,
      snippet,
      printPage,
      source,
      multimedia,
      headline,
      keywords,
      pubDate,
      documentType,
      newsDesk,
      byline,
      typeOfMaterial,
      id,
      wordCount,
      score,
      uri,
    );
  }

  Article fromJson(Map<String, Object> json) {
    return Article.fromJson(json);
  }
}

/// @nodoc
const $Article = _$ArticleTearOff();

/// @nodoc
mixin _$Article {
  @JsonKey(defaultValue: "", name: "web_url")
  String? get webUrl => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get snippet => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "0", name: "print_page")
  String? get printPage => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get source => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: const [])
  List<Multimedia> get multimedia => throw _privateConstructorUsedError;
  Headline get headline => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: const [])
  List<Keyword> get keywords => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "pub_date")
  String? get pubDate => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "document_type")
  String? get documentType => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "news_desk")
  String? get newsDesk => throw _privateConstructorUsedError;
  Byline get byline => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "type_of_material")
  String? get typeOfMaterial => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "_id")
  String? get id => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, name: "word_count")
  int? get wordCount => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0)
  int? get score => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String get uri => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ArticleCopyWith<Article> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ArticleCopyWith<$Res> {
  factory $ArticleCopyWith(Article value, $Res Function(Article) then) =
      _$ArticleCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: "", name: "web_url")
          String? webUrl,
      @JsonKey(defaultValue: "")
          String? snippet,
      @JsonKey(defaultValue: "0", name: "print_page")
          String? printPage,
      @JsonKey(defaultValue: "")
          String? source,
      @JsonKey(defaultValue: const [])
          List<Multimedia> multimedia,
      Headline headline,
      @JsonKey(defaultValue: const [])
          List<Keyword> keywords,
      @JsonKey(defaultValue: "", name: "pub_date")
          String? pubDate,
      @JsonKey(defaultValue: "", name: "document_type")
          String? documentType,
      @JsonKey(defaultValue: "", name: "news_desk")
          String? newsDesk,
      Byline byline,
      @JsonKey(defaultValue: "", name: "type_of_material")
          String? typeOfMaterial,
      @JsonKey(defaultValue: "", name: "_id")
          String? id,
      @JsonKey(defaultValue: 0, name: "word_count")
          int? wordCount,
      @JsonKey(defaultValue: 0)
          int? score,
      @JsonKey(defaultValue: "")
          String uri});

  $HeadlineCopyWith<$Res> get headline;
  $BylineCopyWith<$Res> get byline;
}

/// @nodoc
class _$ArticleCopyWithImpl<$Res> implements $ArticleCopyWith<$Res> {
  _$ArticleCopyWithImpl(this._value, this._then);

  final Article _value;
  // ignore: unused_field
  final $Res Function(Article) _then;

  @override
  $Res call({
    Object? webUrl = freezed,
    Object? snippet = freezed,
    Object? printPage = freezed,
    Object? source = freezed,
    Object? multimedia = freezed,
    Object? headline = freezed,
    Object? keywords = freezed,
    Object? pubDate = freezed,
    Object? documentType = freezed,
    Object? newsDesk = freezed,
    Object? byline = freezed,
    Object? typeOfMaterial = freezed,
    Object? id = freezed,
    Object? wordCount = freezed,
    Object? score = freezed,
    Object? uri = freezed,
  }) {
    return _then(_value.copyWith(
      webUrl: webUrl == freezed
          ? _value.webUrl
          : webUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      snippet: snippet == freezed
          ? _value.snippet
          : snippet // ignore: cast_nullable_to_non_nullable
              as String?,
      printPage: printPage == freezed
          ? _value.printPage
          : printPage // ignore: cast_nullable_to_non_nullable
              as String?,
      source: source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String?,
      multimedia: multimedia == freezed
          ? _value.multimedia
          : multimedia // ignore: cast_nullable_to_non_nullable
              as List<Multimedia>,
      headline: headline == freezed
          ? _value.headline
          : headline // ignore: cast_nullable_to_non_nullable
              as Headline,
      keywords: keywords == freezed
          ? _value.keywords
          : keywords // ignore: cast_nullable_to_non_nullable
              as List<Keyword>,
      pubDate: pubDate == freezed
          ? _value.pubDate
          : pubDate // ignore: cast_nullable_to_non_nullable
              as String?,
      documentType: documentType == freezed
          ? _value.documentType
          : documentType // ignore: cast_nullable_to_non_nullable
              as String?,
      newsDesk: newsDesk == freezed
          ? _value.newsDesk
          : newsDesk // ignore: cast_nullable_to_non_nullable
              as String?,
      byline: byline == freezed
          ? _value.byline
          : byline // ignore: cast_nullable_to_non_nullable
              as Byline,
      typeOfMaterial: typeOfMaterial == freezed
          ? _value.typeOfMaterial
          : typeOfMaterial // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      wordCount: wordCount == freezed
          ? _value.wordCount
          : wordCount // ignore: cast_nullable_to_non_nullable
              as int?,
      score: score == freezed
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as int?,
      uri: uri == freezed
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }

  @override
  $HeadlineCopyWith<$Res> get headline {
    return $HeadlineCopyWith<$Res>(_value.headline, (value) {
      return _then(_value.copyWith(headline: value));
    });
  }

  @override
  $BylineCopyWith<$Res> get byline {
    return $BylineCopyWith<$Res>(_value.byline, (value) {
      return _then(_value.copyWith(byline: value));
    });
  }
}

/// @nodoc
abstract class _$ArticleCopyWith<$Res> implements $ArticleCopyWith<$Res> {
  factory _$ArticleCopyWith(_Article value, $Res Function(_Article) then) =
      __$ArticleCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: "", name: "web_url")
          String? webUrl,
      @JsonKey(defaultValue: "")
          String? snippet,
      @JsonKey(defaultValue: "0", name: "print_page")
          String? printPage,
      @JsonKey(defaultValue: "")
          String? source,
      @JsonKey(defaultValue: const [])
          List<Multimedia> multimedia,
      Headline headline,
      @JsonKey(defaultValue: const [])
          List<Keyword> keywords,
      @JsonKey(defaultValue: "", name: "pub_date")
          String? pubDate,
      @JsonKey(defaultValue: "", name: "document_type")
          String? documentType,
      @JsonKey(defaultValue: "", name: "news_desk")
          String? newsDesk,
      Byline byline,
      @JsonKey(defaultValue: "", name: "type_of_material")
          String? typeOfMaterial,
      @JsonKey(defaultValue: "", name: "_id")
          String? id,
      @JsonKey(defaultValue: 0, name: "word_count")
          int? wordCount,
      @JsonKey(defaultValue: 0)
          int? score,
      @JsonKey(defaultValue: "")
          String uri});

  @override
  $HeadlineCopyWith<$Res> get headline;
  @override
  $BylineCopyWith<$Res> get byline;
}

/// @nodoc
class __$ArticleCopyWithImpl<$Res> extends _$ArticleCopyWithImpl<$Res>
    implements _$ArticleCopyWith<$Res> {
  __$ArticleCopyWithImpl(_Article _value, $Res Function(_Article) _then)
      : super(_value, (v) => _then(v as _Article));

  @override
  _Article get _value => super._value as _Article;

  @override
  $Res call({
    Object? webUrl = freezed,
    Object? snippet = freezed,
    Object? printPage = freezed,
    Object? source = freezed,
    Object? multimedia = freezed,
    Object? headline = freezed,
    Object? keywords = freezed,
    Object? pubDate = freezed,
    Object? documentType = freezed,
    Object? newsDesk = freezed,
    Object? byline = freezed,
    Object? typeOfMaterial = freezed,
    Object? id = freezed,
    Object? wordCount = freezed,
    Object? score = freezed,
    Object? uri = freezed,
  }) {
    return _then(_Article(
      webUrl == freezed
          ? _value.webUrl
          : webUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      snippet == freezed
          ? _value.snippet
          : snippet // ignore: cast_nullable_to_non_nullable
              as String?,
      printPage == freezed
          ? _value.printPage
          : printPage // ignore: cast_nullable_to_non_nullable
              as String?,
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String?,
      multimedia == freezed
          ? _value.multimedia
          : multimedia // ignore: cast_nullable_to_non_nullable
              as List<Multimedia>,
      headline == freezed
          ? _value.headline
          : headline // ignore: cast_nullable_to_non_nullable
              as Headline,
      keywords == freezed
          ? _value.keywords
          : keywords // ignore: cast_nullable_to_non_nullable
              as List<Keyword>,
      pubDate == freezed
          ? _value.pubDate
          : pubDate // ignore: cast_nullable_to_non_nullable
              as String?,
      documentType == freezed
          ? _value.documentType
          : documentType // ignore: cast_nullable_to_non_nullable
              as String?,
      newsDesk == freezed
          ? _value.newsDesk
          : newsDesk // ignore: cast_nullable_to_non_nullable
              as String?,
      byline == freezed
          ? _value.byline
          : byline // ignore: cast_nullable_to_non_nullable
              as Byline,
      typeOfMaterial == freezed
          ? _value.typeOfMaterial
          : typeOfMaterial // ignore: cast_nullable_to_non_nullable
              as String?,
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      wordCount == freezed
          ? _value.wordCount
          : wordCount // ignore: cast_nullable_to_non_nullable
              as int?,
      score == freezed
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as int?,
      uri == freezed
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Article implements _Article {
  _$_Article(
      @JsonKey(defaultValue: "", name: "web_url") this.webUrl,
      @JsonKey(defaultValue: "") this.snippet,
      @JsonKey(defaultValue: "0", name: "print_page") this.printPage,
      @JsonKey(defaultValue: "") this.source,
      @JsonKey(defaultValue: const []) this.multimedia,
      this.headline,
      @JsonKey(defaultValue: const []) this.keywords,
      @JsonKey(defaultValue: "", name: "pub_date") this.pubDate,
      @JsonKey(defaultValue: "", name: "document_type") this.documentType,
      @JsonKey(defaultValue: "", name: "news_desk") this.newsDesk,
      this.byline,
      @JsonKey(defaultValue: "", name: "type_of_material") this.typeOfMaterial,
      @JsonKey(defaultValue: "", name: "_id") this.id,
      @JsonKey(defaultValue: 0, name: "word_count") this.wordCount,
      @JsonKey(defaultValue: 0) this.score,
      @JsonKey(defaultValue: "") this.uri);

  factory _$_Article.fromJson(Map<String, dynamic> json) =>
      _$_$_ArticleFromJson(json);

  @override
  @JsonKey(defaultValue: "", name: "web_url")
  final String? webUrl;
  @override
  @JsonKey(defaultValue: "")
  final String? snippet;
  @override
  @JsonKey(defaultValue: "0", name: "print_page")
  final String? printPage;
  @override
  @JsonKey(defaultValue: "")
  final String? source;
  @override
  @JsonKey(defaultValue: const [])
  final List<Multimedia> multimedia;
  @override
  final Headline headline;
  @override
  @JsonKey(defaultValue: const [])
  final List<Keyword> keywords;
  @override
  @JsonKey(defaultValue: "", name: "pub_date")
  final String? pubDate;
  @override
  @JsonKey(defaultValue: "", name: "document_type")
  final String? documentType;
  @override
  @JsonKey(defaultValue: "", name: "news_desk")
  final String? newsDesk;
  @override
  final Byline byline;
  @override
  @JsonKey(defaultValue: "", name: "type_of_material")
  final String? typeOfMaterial;
  @override
  @JsonKey(defaultValue: "", name: "_id")
  final String? id;
  @override
  @JsonKey(defaultValue: 0, name: "word_count")
  final int? wordCount;
  @override
  @JsonKey(defaultValue: 0)
  final int? score;
  @override
  @JsonKey(defaultValue: "")
  final String uri;

  @override
  String toString() {
    return 'Article(webUrl: $webUrl, snippet: $snippet, printPage: $printPage, source: $source, multimedia: $multimedia, headline: $headline, keywords: $keywords, pubDate: $pubDate, documentType: $documentType, newsDesk: $newsDesk, byline: $byline, typeOfMaterial: $typeOfMaterial, id: $id, wordCount: $wordCount, score: $score, uri: $uri)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Article &&
            (identical(other.webUrl, webUrl) ||
                const DeepCollectionEquality().equals(other.webUrl, webUrl)) &&
            (identical(other.snippet, snippet) ||
                const DeepCollectionEquality()
                    .equals(other.snippet, snippet)) &&
            (identical(other.printPage, printPage) ||
                const DeepCollectionEquality()
                    .equals(other.printPage, printPage)) &&
            (identical(other.source, source) ||
                const DeepCollectionEquality().equals(other.source, source)) &&
            (identical(other.multimedia, multimedia) ||
                const DeepCollectionEquality()
                    .equals(other.multimedia, multimedia)) &&
            (identical(other.headline, headline) ||
                const DeepCollectionEquality()
                    .equals(other.headline, headline)) &&
            (identical(other.keywords, keywords) ||
                const DeepCollectionEquality()
                    .equals(other.keywords, keywords)) &&
            (identical(other.pubDate, pubDate) ||
                const DeepCollectionEquality()
                    .equals(other.pubDate, pubDate)) &&
            (identical(other.documentType, documentType) ||
                const DeepCollectionEquality()
                    .equals(other.documentType, documentType)) &&
            (identical(other.newsDesk, newsDesk) ||
                const DeepCollectionEquality()
                    .equals(other.newsDesk, newsDesk)) &&
            (identical(other.byline, byline) ||
                const DeepCollectionEquality().equals(other.byline, byline)) &&
            (identical(other.typeOfMaterial, typeOfMaterial) ||
                const DeepCollectionEquality()
                    .equals(other.typeOfMaterial, typeOfMaterial)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.wordCount, wordCount) ||
                const DeepCollectionEquality()
                    .equals(other.wordCount, wordCount)) &&
            (identical(other.score, score) ||
                const DeepCollectionEquality().equals(other.score, score)) &&
            (identical(other.uri, uri) ||
                const DeepCollectionEquality().equals(other.uri, uri)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(webUrl) ^
      const DeepCollectionEquality().hash(snippet) ^
      const DeepCollectionEquality().hash(printPage) ^
      const DeepCollectionEquality().hash(source) ^
      const DeepCollectionEquality().hash(multimedia) ^
      const DeepCollectionEquality().hash(headline) ^
      const DeepCollectionEquality().hash(keywords) ^
      const DeepCollectionEquality().hash(pubDate) ^
      const DeepCollectionEquality().hash(documentType) ^
      const DeepCollectionEquality().hash(newsDesk) ^
      const DeepCollectionEquality().hash(byline) ^
      const DeepCollectionEquality().hash(typeOfMaterial) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(wordCount) ^
      const DeepCollectionEquality().hash(score) ^
      const DeepCollectionEquality().hash(uri);

  @JsonKey(ignore: true)
  @override
  _$ArticleCopyWith<_Article> get copyWith =>
      __$ArticleCopyWithImpl<_Article>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ArticleToJson(this);
  }
}

abstract class _Article implements Article {
  factory _Article(
      @JsonKey(defaultValue: "", name: "web_url")
          String? webUrl,
      @JsonKey(defaultValue: "")
          String? snippet,
      @JsonKey(defaultValue: "0", name: "print_page")
          String? printPage,
      @JsonKey(defaultValue: "")
          String? source,
      @JsonKey(defaultValue: const [])
          List<Multimedia> multimedia,
      Headline headline,
      @JsonKey(defaultValue: const [])
          List<Keyword> keywords,
      @JsonKey(defaultValue: "", name: "pub_date")
          String? pubDate,
      @JsonKey(defaultValue: "", name: "document_type")
          String? documentType,
      @JsonKey(defaultValue: "", name: "news_desk")
          String? newsDesk,
      Byline byline,
      @JsonKey(defaultValue: "", name: "type_of_material")
          String? typeOfMaterial,
      @JsonKey(defaultValue: "", name: "_id")
          String? id,
      @JsonKey(defaultValue: 0, name: "word_count")
          int? wordCount,
      @JsonKey(defaultValue: 0)
          int? score,
      @JsonKey(defaultValue: "")
          String uri) = _$_Article;

  factory _Article.fromJson(Map<String, dynamic> json) = _$_Article.fromJson;

  @override
  @JsonKey(defaultValue: "", name: "web_url")
  String? get webUrl => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get snippet => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "0", name: "print_page")
  String? get printPage => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get source => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: const [])
  List<Multimedia> get multimedia => throw _privateConstructorUsedError;
  @override
  Headline get headline => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: const [])
  List<Keyword> get keywords => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "pub_date")
  String? get pubDate => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "document_type")
  String? get documentType => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "news_desk")
  String? get newsDesk => throw _privateConstructorUsedError;
  @override
  Byline get byline => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "type_of_material")
  String? get typeOfMaterial => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "_id")
  String? get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: 0, name: "word_count")
  int? get wordCount => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: 0)
  int? get score => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String get uri => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ArticleCopyWith<_Article> get copyWith =>
      throw _privateConstructorUsedError;
}
