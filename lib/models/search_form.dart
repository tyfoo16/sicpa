//  String? beginDate,
//     String? endDate,
//     bool? facet,
//     FacetFields? facetFields,
//     bool? facetFilter,
//     String? fieldList,
//     String? filterQuery,
//     int? page,
//     String? query,
//     SortMethod? sort,

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/sort_method.dart';

import 'facet_fields.dart';

part 'search_form.freezed.dart';

@freezed
class SearchForm with _$SearchForm {
  factory SearchForm(
    String? beginDate,
    String? endDate,
    bool? facet,
    FacetFields? facetFields,
    bool? facetFilter,
    String? fieldList,
    String? filterQuery,
    int? page,
    String query,
    SortMethod? sort,
  ) = _SearchForm;

  // factory SearchForm.fromJson(Map<String, dynamic> json) =>
  //     _$SearchFormFromJson(json);
}
