enum ArticlesType {
  MostViewed,
  MostEmailed,
  MostShared,
}

extension ArticlesTypeExtension on ArticlesType {
  String get value {
    switch (this) {
      case ArticlesType.MostViewed:
        return "Most Viewed";
      case ArticlesType.MostEmailed:
        return "Most Emailed";
      case ArticlesType.MostShared:
        return "Most Shared";
    }
  }
}
