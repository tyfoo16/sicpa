// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'person.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Person _$PersonFromJson(Map<String, dynamic> json) {
  return _Person.fromJson(json);
}

/// @nodoc
class _$PersonTearOff {
  const _$PersonTearOff();

  _Person call(
      @JsonKey(defaultValue: "") String? firstname,
      @JsonKey(defaultValue: "") String? middlename,
      @JsonKey(defaultValue: "") String? lastname,
      @JsonKey(defaultValue: "") String? qualifier,
      @JsonKey(defaultValue: "") String? title,
      String role,
      String organization,
      int rank) {
    return _Person(
      firstname,
      middlename,
      lastname,
      qualifier,
      title,
      role,
      organization,
      rank,
    );
  }

  Person fromJson(Map<String, Object> json) {
    return Person.fromJson(json);
  }
}

/// @nodoc
const $Person = _$PersonTearOff();

/// @nodoc
mixin _$Person {
  @JsonKey(defaultValue: "")
  String? get firstname => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get middlename => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get lastname => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get qualifier => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get title => throw _privateConstructorUsedError;
  String get role => throw _privateConstructorUsedError;
  String get organization => throw _privateConstructorUsedError;
  int get rank => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PersonCopyWith<Person> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonCopyWith<$Res> {
  factory $PersonCopyWith(Person value, $Res Function(Person) then) =
      _$PersonCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: "") String? firstname,
      @JsonKey(defaultValue: "") String? middlename,
      @JsonKey(defaultValue: "") String? lastname,
      @JsonKey(defaultValue: "") String? qualifier,
      @JsonKey(defaultValue: "") String? title,
      String role,
      String organization,
      int rank});
}

/// @nodoc
class _$PersonCopyWithImpl<$Res> implements $PersonCopyWith<$Res> {
  _$PersonCopyWithImpl(this._value, this._then);

  final Person _value;
  // ignore: unused_field
  final $Res Function(Person) _then;

  @override
  $Res call({
    Object? firstname = freezed,
    Object? middlename = freezed,
    Object? lastname = freezed,
    Object? qualifier = freezed,
    Object? title = freezed,
    Object? role = freezed,
    Object? organization = freezed,
    Object? rank = freezed,
  }) {
    return _then(_value.copyWith(
      firstname: firstname == freezed
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String?,
      middlename: middlename == freezed
          ? _value.middlename
          : middlename // ignore: cast_nullable_to_non_nullable
              as String?,
      lastname: lastname == freezed
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String?,
      qualifier: qualifier == freezed
          ? _value.qualifier
          : qualifier // ignore: cast_nullable_to_non_nullable
              as String?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      role: role == freezed
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      organization: organization == freezed
          ? _value.organization
          : organization // ignore: cast_nullable_to_non_nullable
              as String,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$PersonCopyWith<$Res> implements $PersonCopyWith<$Res> {
  factory _$PersonCopyWith(_Person value, $Res Function(_Person) then) =
      __$PersonCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: "") String? firstname,
      @JsonKey(defaultValue: "") String? middlename,
      @JsonKey(defaultValue: "") String? lastname,
      @JsonKey(defaultValue: "") String? qualifier,
      @JsonKey(defaultValue: "") String? title,
      String role,
      String organization,
      int rank});
}

/// @nodoc
class __$PersonCopyWithImpl<$Res> extends _$PersonCopyWithImpl<$Res>
    implements _$PersonCopyWith<$Res> {
  __$PersonCopyWithImpl(_Person _value, $Res Function(_Person) _then)
      : super(_value, (v) => _then(v as _Person));

  @override
  _Person get _value => super._value as _Person;

  @override
  $Res call({
    Object? firstname = freezed,
    Object? middlename = freezed,
    Object? lastname = freezed,
    Object? qualifier = freezed,
    Object? title = freezed,
    Object? role = freezed,
    Object? organization = freezed,
    Object? rank = freezed,
  }) {
    return _then(_Person(
      firstname == freezed
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String?,
      middlename == freezed
          ? _value.middlename
          : middlename // ignore: cast_nullable_to_non_nullable
              as String?,
      lastname == freezed
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String?,
      qualifier == freezed
          ? _value.qualifier
          : qualifier // ignore: cast_nullable_to_non_nullable
              as String?,
      title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      role == freezed
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as String,
      organization == freezed
          ? _value.organization
          : organization // ignore: cast_nullable_to_non_nullable
              as String,
      rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Person implements _Person {
  _$_Person(
      @JsonKey(defaultValue: "") this.firstname,
      @JsonKey(defaultValue: "") this.middlename,
      @JsonKey(defaultValue: "") this.lastname,
      @JsonKey(defaultValue: "") this.qualifier,
      @JsonKey(defaultValue: "") this.title,
      this.role,
      this.organization,
      this.rank);

  factory _$_Person.fromJson(Map<String, dynamic> json) =>
      _$_$_PersonFromJson(json);

  @override
  @JsonKey(defaultValue: "")
  final String? firstname;
  @override
  @JsonKey(defaultValue: "")
  final String? middlename;
  @override
  @JsonKey(defaultValue: "")
  final String? lastname;
  @override
  @JsonKey(defaultValue: "")
  final String? qualifier;
  @override
  @JsonKey(defaultValue: "")
  final String? title;
  @override
  final String role;
  @override
  final String organization;
  @override
  final int rank;

  @override
  String toString() {
    return 'Person(firstname: $firstname, middlename: $middlename, lastname: $lastname, qualifier: $qualifier, title: $title, role: $role, organization: $organization, rank: $rank)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Person &&
            (identical(other.firstname, firstname) ||
                const DeepCollectionEquality()
                    .equals(other.firstname, firstname)) &&
            (identical(other.middlename, middlename) ||
                const DeepCollectionEquality()
                    .equals(other.middlename, middlename)) &&
            (identical(other.lastname, lastname) ||
                const DeepCollectionEquality()
                    .equals(other.lastname, lastname)) &&
            (identical(other.qualifier, qualifier) ||
                const DeepCollectionEquality()
                    .equals(other.qualifier, qualifier)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.role, role) ||
                const DeepCollectionEquality().equals(other.role, role)) &&
            (identical(other.organization, organization) ||
                const DeepCollectionEquality()
                    .equals(other.organization, organization)) &&
            (identical(other.rank, rank) ||
                const DeepCollectionEquality().equals(other.rank, rank)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(firstname) ^
      const DeepCollectionEquality().hash(middlename) ^
      const DeepCollectionEquality().hash(lastname) ^
      const DeepCollectionEquality().hash(qualifier) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(role) ^
      const DeepCollectionEquality().hash(organization) ^
      const DeepCollectionEquality().hash(rank);

  @JsonKey(ignore: true)
  @override
  _$PersonCopyWith<_Person> get copyWith =>
      __$PersonCopyWithImpl<_Person>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PersonToJson(this);
  }
}

abstract class _Person implements Person {
  factory _Person(
      @JsonKey(defaultValue: "") String? firstname,
      @JsonKey(defaultValue: "") String? middlename,
      @JsonKey(defaultValue: "") String? lastname,
      @JsonKey(defaultValue: "") String? qualifier,
      @JsonKey(defaultValue: "") String? title,
      String role,
      String organization,
      int rank) = _$_Person;

  factory _Person.fromJson(Map<String, dynamic> json) = _$_Person.fromJson;

  @override
  @JsonKey(defaultValue: "")
  String? get firstname => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get middlename => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get lastname => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get qualifier => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get title => throw _privateConstructorUsedError;
  @override
  String get role => throw _privateConstructorUsedError;
  @override
  String get organization => throw _privateConstructorUsedError;
  @override
  int get rank => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PersonCopyWith<_Person> get copyWith => throw _privateConstructorUsedError;
}
