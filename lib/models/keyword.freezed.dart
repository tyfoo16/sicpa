// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'keyword.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Keyword _$KeywordFromJson(Map<String, dynamic> json) {
  return _Keyword.fromJson(json);
}

/// @nodoc
class _$KeywordTearOff {
  const _$KeywordTearOff();

  _Keyword call(String name, String value, int rank, String major) {
    return _Keyword(
      name,
      value,
      rank,
      major,
    );
  }

  Keyword fromJson(Map<String, Object> json) {
    return Keyword.fromJson(json);
  }
}

/// @nodoc
const $Keyword = _$KeywordTearOff();

/// @nodoc
mixin _$Keyword {
  String get name => throw _privateConstructorUsedError;
  String get value => throw _privateConstructorUsedError;
  int get rank => throw _privateConstructorUsedError;
  String get major => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $KeywordCopyWith<Keyword> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $KeywordCopyWith<$Res> {
  factory $KeywordCopyWith(Keyword value, $Res Function(Keyword) then) =
      _$KeywordCopyWithImpl<$Res>;
  $Res call({String name, String value, int rank, String major});
}

/// @nodoc
class _$KeywordCopyWithImpl<$Res> implements $KeywordCopyWith<$Res> {
  _$KeywordCopyWithImpl(this._value, this._then);

  final Keyword _value;
  // ignore: unused_field
  final $Res Function(Keyword) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? value = freezed,
    Object? rank = freezed,
    Object? major = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
      major: major == freezed
          ? _value.major
          : major // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$KeywordCopyWith<$Res> implements $KeywordCopyWith<$Res> {
  factory _$KeywordCopyWith(_Keyword value, $Res Function(_Keyword) then) =
      __$KeywordCopyWithImpl<$Res>;
  @override
  $Res call({String name, String value, int rank, String major});
}

/// @nodoc
class __$KeywordCopyWithImpl<$Res> extends _$KeywordCopyWithImpl<$Res>
    implements _$KeywordCopyWith<$Res> {
  __$KeywordCopyWithImpl(_Keyword _value, $Res Function(_Keyword) _then)
      : super(_value, (v) => _then(v as _Keyword));

  @override
  _Keyword get _value => super._value as _Keyword;

  @override
  $Res call({
    Object? name = freezed,
    Object? value = freezed,
    Object? rank = freezed,
    Object? major = freezed,
  }) {
    return _then(_Keyword(
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
      major == freezed
          ? _value.major
          : major // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Keyword implements _Keyword {
  _$_Keyword(this.name, this.value, this.rank, this.major);

  factory _$_Keyword.fromJson(Map<String, dynamic> json) =>
      _$_$_KeywordFromJson(json);

  @override
  final String name;
  @override
  final String value;
  @override
  final int rank;
  @override
  final String major;

  @override
  String toString() {
    return 'Keyword(name: $name, value: $value, rank: $rank, major: $major)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Keyword &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)) &&
            (identical(other.rank, rank) ||
                const DeepCollectionEquality().equals(other.rank, rank)) &&
            (identical(other.major, major) ||
                const DeepCollectionEquality().equals(other.major, major)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(value) ^
      const DeepCollectionEquality().hash(rank) ^
      const DeepCollectionEquality().hash(major);

  @JsonKey(ignore: true)
  @override
  _$KeywordCopyWith<_Keyword> get copyWith =>
      __$KeywordCopyWithImpl<_Keyword>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_KeywordToJson(this);
  }
}

abstract class _Keyword implements Keyword {
  factory _Keyword(String name, String value, int rank, String major) =
      _$_Keyword;

  factory _Keyword.fromJson(Map<String, dynamic> json) = _$_Keyword.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  String get value => throw _privateConstructorUsedError;
  @override
  int get rank => throw _privateConstructorUsedError;
  @override
  String get major => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$KeywordCopyWith<_Keyword> get copyWith =>
      throw _privateConstructorUsedError;
}
