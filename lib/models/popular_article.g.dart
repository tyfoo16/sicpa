// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_article.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PopularArticle _$_$_PopularArticleFromJson(Map<String, dynamic> json) {
  return _$_PopularArticle(
    json['uri'] as String,
    json['url'] as String,
    json['id'] as int,
    json['asset_id'] as int,
    json['source'] as String,
    json['published_date'] as String,
    json['updated'] as String,
    json['section'] as String,
    json['subsection'] as String,
    json['nytdsection'] as String,
    json['adx_keywords'] as String,
    json['column'] as String?,
    json['byline'] as String,
    json['type'] as String,
    json['title'] as String,
    json['abstract'] as String,
    (json['des_facet'] as List<dynamic>).map((e) => e as String).toList(),
    (json['org_facet'] as List<dynamic>).map((e) => e as String).toList(),
    (json['per_facet'] as List<dynamic>).map((e) => e as String).toList(),
    (json['geo_facet'] as List<dynamic>).map((e) => e as String).toList(),
    (json['media'] as List<dynamic>)
        .map((e) => Media.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['eta_id'] as int,
  );
}

Map<String, dynamic> _$_$_PopularArticleToJson(_$_PopularArticle instance) =>
    <String, dynamic>{
      'uri': instance.uri,
      'url': instance.url,
      'id': instance.id,
      'asset_id': instance.assetId,
      'source': instance.source,
      'published_date': instance.publishedDate,
      'updated': instance.updated,
      'section': instance.section,
      'subsection': instance.subsection,
      'nytdsection': instance.nytdsection,
      'adx_keywords': instance.adxKeywords,
      'column': instance.column,
      'byline': instance.byline,
      'type': instance.type,
      'title': instance.title,
      'abstract': instance.abstractStr,
      'des_facet': instance.desFacet,
      'org_facet': instance.orgFacet,
      'per_facet': instance.perFacet,
      'geo_facet': instance.geoFacet,
      'media': instance.media.map((e) => e.toJson()).toList(),
      'eta_id': instance.etaId,
    };
