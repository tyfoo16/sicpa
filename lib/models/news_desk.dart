enum NewsDesk {
  AdventureSports,
  ArtsAndLeisure,
  Arts,
  Automobiles,
  Blogs,
  Books,
  Booming,
  BusinessDay,
  Business,
  Cars,
  Circuits,
  Classifieds,
  Connecticut,
  CrosswordsAndGames,
  Culture,
  DealBook,
  Dining,
  Editorial,
  Education,
  Energy,
  Entrepreneurs,
  Environment,
  Escapes,
  FashionAndStyle,
  Fashion,
  Favorites,
  Financial,
  Flight,
  Food,
  Foreign,
  Generations,
  Giving,
  GlobalHome,
  HealthAndFitness,
  Health,
  HomeAndGarden,
  Home,
  Jobs,
  Key,
  Letters,
  LongIsland,
  Magazine,
  MarketPlace,
  Media,
  MensHealth,
  Metro,
  Metropolitan,
  Movies,
  Museums,
  National,
  Nesting,
  Obits,
  Obituaries,
  Obituary,
  OpEd,
  Opinion,
  Outlook,
  PersonalInvesting,
  PersonalTech,
  Play,
  Politics,
  Regionals,
  Retail,
  Retirement,
  Science,
  SmallBusiness,
  Society,
  Sports,
  Style,
  SundayBusiness,
  SundayReview,
  SundayStyles,
  TMagazine,
  TStyle,
  Technology,
  Teens,
  Television,
  TheArts,
  TheBusinessOfGreen,
  TheCityDesk,
  TheCity,
  TheMarathon,
  TheMillennium,
  TheNaturalWorld,
  TheUpshot,
  TheWeekend,
  TheYearInPictures,
  Theater,
  ThenAndNow,
  ThursdayStyles,
  TimesTopics,
  Travel,
  US,
  Universal,
  Upshot,
  UrbanEye,
  Vacation,
  Washington,
  Wealth,
  Weather,
  WeekinReview,
  Week,
  Weekend,
  Westchester,
  WirelessLiving,
  WomensHealth,
  Working,
  Workplace,
  World,
  YourMoney,
}

extension NewsDeskExtension on NewsDesk {
  String get value {
    switch (this) {
      case NewsDesk.AdventureSports:
        return "Adventure Sports";
      case NewsDesk.ArtsAndLeisure:
        return "Arts & Leisure";
      case NewsDesk.Arts:
        return "Arts";
      case NewsDesk.Automobiles:
        return "Automobiles";
      case NewsDesk.Blogs:
        return "Blogs";
      case NewsDesk.Books:
        return "Books";
      case NewsDesk.Booming:
        return "Booming";
      case NewsDesk.BusinessDay:
        return "Business Day";
      case NewsDesk.Business:
        return "Business";
      case NewsDesk.Cars:
        return "Cars";
      case NewsDesk.Circuits:
        return "Circuits";
      case NewsDesk.Classifieds:
        return "Classifieds";
      case NewsDesk.Connecticut:
        return "Connecticut";
      case NewsDesk.CrosswordsAndGames:
        return "Crosswords \& Games";
      case NewsDesk.Culture:
        return "Culture";
      case NewsDesk.DealBook:
        return "Dealbook";
      case NewsDesk.Dining:
        return "Dining";
      case NewsDesk.Editorial:
        return "Editorial";
      case NewsDesk.Education:
        return "Education";
      case NewsDesk.Energy:
        return "Energy";
      case NewsDesk.Entrepreneurs:
        return "Entrepreneurs";
      case NewsDesk.Environment:
        return "Environment";
      case NewsDesk.Escapes:
        return "Escapes";
      case NewsDesk.FashionAndStyle:
        return "Fashion \& Style";
      case NewsDesk.Fashion:
        return "Fashion";
      case NewsDesk.Favorites:
        return "Favorites";
      case NewsDesk.Financial:
        return "Financial";
      case NewsDesk.Flight:
        return "Flight";
      case NewsDesk.Food:
        return "Food";
      case NewsDesk.Foreign:
        return "Foreign";
      case NewsDesk.Generations:
        return "Generations";
      case NewsDesk.Giving:
        return "Giving";
      case NewsDesk.GlobalHome:
        return "Global Home";
      case NewsDesk.HealthAndFitness:
        return "Health \& Fitness";
      case NewsDesk.Health:
        return "Health";
      case NewsDesk.HomeAndGarden:
        return "Home & Garden";
      case NewsDesk.Home:
        return "Home";
      case NewsDesk.Jobs:
        return "Jobs";
      case NewsDesk.Key:
        return "Key";
      case NewsDesk.Letters:
        return "Letters";
      case NewsDesk.LongIsland:
        return "Long Island";
      case NewsDesk.Magazine:
        return "Magazine";
      case NewsDesk.MarketPlace:
        return "MarketPlace";
      case NewsDesk.Media:
        return "Media";
      case NewsDesk.MensHealth:
        return "Men\'s Health";
      case NewsDesk.Metro:
        return "Metro";
      case NewsDesk.Metropolitan:
        return "Metropolitan";
      case NewsDesk.Movies:
        return "Movies";
      case NewsDesk.Museums:
        return "Museums";
      case NewsDesk.National:
        return "National";
      case NewsDesk.Nesting:
        return "Nesting";
      case NewsDesk.Obits:
        return "Obits";
      case NewsDesk.Obituaries:
        return "Obituaries";
      case NewsDesk.Obituary:
        return "Obituary";
      case NewsDesk.OpEd:
        return "OpEd";
      case NewsDesk.Opinion:
        return "Opinion";
      case NewsDesk.Outlook:
        return "Outlook";
      case NewsDesk.PersonalInvesting:
        return "Personal Investing";
      case NewsDesk.PersonalTech:
        return "Personal Tech";
      case NewsDesk.Play:
        return "Play";
      case NewsDesk.Politics:
        return "Politics";
      case NewsDesk.Regionals:
        return "Regionals";
      case NewsDesk.Retail:
        return "Retail";
      case NewsDesk.Retirement:
        return "Retirement";
      case NewsDesk.Science:
        return "Science";
      case NewsDesk.SmallBusiness:
        return "Small Business";
      case NewsDesk.Society:
        return "Society";
      case NewsDesk.Sports:
        return "Sports";
      case NewsDesk.Style:
        return "Style";
      case NewsDesk.SundayBusiness:
        return "Sunday Business";
      case NewsDesk.SundayReview:
        return "Sunday Review";
      case NewsDesk.SundayStyles:
        return "Sunday Styles";
      case NewsDesk.TMagazine:
        return "T Magazine";
      case NewsDesk.TStyle:
        return "T Style";
      case NewsDesk.Technology:
        return "Technology";
      case NewsDesk.Teens:
        return "Teens";
      case NewsDesk.Television:
        return "Television";
      case NewsDesk.TheArts:
        return "The Arts";
      case NewsDesk.TheBusinessOfGreen:
        return "The Business of Green";
      case NewsDesk.TheCityDesk:
        return "The City Desk";
      case NewsDesk.TheCity:
        return "The City";
      case NewsDesk.TheMarathon:
        return "The Marathon";
      case NewsDesk.TheMillennium:
        return "The Millennium";
      case NewsDesk.TheNaturalWorld:
        return "The Natural World";
      case NewsDesk.TheUpshot:
        return "The Upshot";
      case NewsDesk.TheWeekend:
        return "The Weekend";
      case NewsDesk.TheYearInPictures:
        return "The Year in Pictures";
      case NewsDesk.Theater:
        return "Theater";
      case NewsDesk.ThenAndNow:
        return "Then \& Now";
      case NewsDesk.ThursdayStyles:
        return "Thursday Styles";
      case NewsDesk.TimesTopics:
        return "Times Topics";
      case NewsDesk.Travel:
        return "Travel";
      case NewsDesk.US:
        return "U.S.";
      case NewsDesk.Universal:
        return "Universal";
      case NewsDesk.Upshot:
        return "Upshot";
      case NewsDesk.UrbanEye:
        return "UrbanEye";
      case NewsDesk.Vacation:
        return "Vacation";
      case NewsDesk.Washington:
        return "Washington";
      case NewsDesk.Wealth:
        return "Wealth";
      case NewsDesk.Weather:
        return "Weather";
      case NewsDesk.WeekinReview:
        return "Week in Review";
      case NewsDesk.Week:
        return "Week";
      case NewsDesk.Weekend:
        return "Weekend";
      case NewsDesk.Westchester:
        return "Westchester";
      case NewsDesk.WirelessLiving:
        return "Wireless Living";
      case NewsDesk.WomensHealth:
        return "Women\'s Health";
      case NewsDesk.Working:
        return "Working";
      case NewsDesk.Workplace:
        return "Workplace";
      case NewsDesk.World:
        return "World";
      case NewsDesk.YourMoney:
        return "Your Money";
    }
  }
}
