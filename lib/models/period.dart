enum Period { OneDay, SevenDays, ThirtyDays }

extension PeriodExtension on Period {
  int get days {
    switch (this) {
      case Period.OneDay:
        return 1;
      case Period.SevenDays:
        return 7;
      case Period.ThirtyDays:
        return 30;
    }
  }
}
