import 'article.dart';

class ArticlesReponse {
  final int hits;
  final int offset;
  final int time;
  final List<Article> docs;

  ArticlesReponse(this.docs, this.hits, this.offset, this.time);
  ArticlesReponse.fromJson(Map<String, dynamic> json)
      : docs = (json["response"]["docs"] as List<dynamic>)
            .map((e) => Article.fromJson(e))
            .toList(),
        hits = json["response"]["meta"]["hits"] as int,
        offset = json["response"]["meta"]["offset"] as int,
        time = json["response"]["meta"]["time"] as int;
}
