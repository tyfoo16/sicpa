// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keyword.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Keyword _$_$_KeywordFromJson(Map<String, dynamic> json) {
  return _$_Keyword(
    json['name'] as String,
    json['value'] as String,
    json['rank'] as int,
    json['major'] as String,
  );
}

Map<String, dynamic> _$_$_KeywordToJson(_$_Keyword instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
      'rank': instance.rank,
      'major': instance.major,
    };
