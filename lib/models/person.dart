import 'package:freezed_annotation/freezed_annotation.dart';

part 'person.freezed.dart';
part 'person.g.dart';

// "firstname": "Elian",
// "middlename": null,
// "lastname": "Peltier",
// "qualifier": null,
// "title": null,
// "role": "reported",
// "organization": "",
// "rank": 1
@freezed
class Person with _$Person {
  factory Person(
    @JsonKey(defaultValue: "") String? firstname,
    @JsonKey(defaultValue: "") String? middlename,
    @JsonKey(defaultValue: "") String? lastname,
    @JsonKey(defaultValue: "") String? qualifier,
    @JsonKey(defaultValue: "") String? title,
    String role,
    String organization,
    int rank,
  ) = _Person;

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
}
