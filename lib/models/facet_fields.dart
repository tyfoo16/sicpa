enum FacetFields {
  DayOfWeek,
  DocumentType,
  Ingredients,
  NewsDesk,
  PubMonth,
  PubYear,
  SectionName,
  Source,
  SubsectionName,
  TypeOfMaterial,
}

// day_of_week, document_type, ingredients, news_desk, pub_month, pub_year, section_name, source, subsection_name, type_of_material
extension FacetFieldsExtension on FacetFields {
  String get value {
    switch (this) {
      case FacetFields.DayOfWeek:
        return "day_of_week";
      case FacetFields.DocumentType:
        return "document_type";
      case FacetFields.Ingredients:
        return "ingredients";
      case FacetFields.NewsDesk:
        return "news_desk";
      case FacetFields.PubMonth:
        return "pub_month";
      case FacetFields.PubYear:
        return "pub_year";
      case FacetFields.SectionName:
        return "section_name";
      case FacetFields.Source:
        return "source";
      case FacetFields.SubsectionName:
        return "subsection_name";
      case FacetFields.TypeOfMaterial:
        return "type_of_material";
    }
  }
}
