import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/media_metadata.dart';

part 'media.freezed.dart';
part 'media.g.dart';

@freezed
class Media with _$Media {
  factory Media(
    String type,
    String subtype,
    String caption,
    String copyright,
    @JsonKey(name: 'approved_for_syndication') int approvedForSyndication,
    @JsonKey(name: 'media-metadata') List<MediaMetadata> mediaMetadata,
  ) = _Media;

  factory Media.fromJson(Map<String, dynamic> json) => _$MediaFromJson(json);
}

extension MediaHelper on Media {
  bool get approvedForSyndicationBool {
    return this.approvedForSyndication == 1 ? true : false;
  }
}
