// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'headline.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Headline _$HeadlineFromJson(Map<String, dynamic> json) {
  return _Headline.fromJson(json);
}

/// @nodoc
class _$HeadlineTearOff {
  const _$HeadlineTearOff();

  _Headline call(
      String main,
      @JsonKey(defaultValue: "") String? kicker,
      @JsonKey(defaultValue: "", name: "content_kicker") String? contentKicker,
      @JsonKey(defaultValue: "", name: "print_headline") String? printHeadline,
      @JsonKey(defaultValue: "") String? name,
      @JsonKey(defaultValue: "") String? seo,
      @JsonKey(defaultValue: "") String? sub) {
    return _Headline(
      main,
      kicker,
      contentKicker,
      printHeadline,
      name,
      seo,
      sub,
    );
  }

  Headline fromJson(Map<String, Object> json) {
    return Headline.fromJson(json);
  }
}

/// @nodoc
const $Headline = _$HeadlineTearOff();

/// @nodoc
mixin _$Headline {
  String get main => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get kicker => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "content_kicker")
  String? get contentKicker => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", name: "print_headline")
  String? get printHeadline => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get seo => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "")
  String? get sub => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HeadlineCopyWith<Headline> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HeadlineCopyWith<$Res> {
  factory $HeadlineCopyWith(Headline value, $Res Function(Headline) then) =
      _$HeadlineCopyWithImpl<$Res>;
  $Res call(
      {String main,
      @JsonKey(defaultValue: "") String? kicker,
      @JsonKey(defaultValue: "", name: "content_kicker") String? contentKicker,
      @JsonKey(defaultValue: "", name: "print_headline") String? printHeadline,
      @JsonKey(defaultValue: "") String? name,
      @JsonKey(defaultValue: "") String? seo,
      @JsonKey(defaultValue: "") String? sub});
}

/// @nodoc
class _$HeadlineCopyWithImpl<$Res> implements $HeadlineCopyWith<$Res> {
  _$HeadlineCopyWithImpl(this._value, this._then);

  final Headline _value;
  // ignore: unused_field
  final $Res Function(Headline) _then;

  @override
  $Res call({
    Object? main = freezed,
    Object? kicker = freezed,
    Object? contentKicker = freezed,
    Object? printHeadline = freezed,
    Object? name = freezed,
    Object? seo = freezed,
    Object? sub = freezed,
  }) {
    return _then(_value.copyWith(
      main: main == freezed
          ? _value.main
          : main // ignore: cast_nullable_to_non_nullable
              as String,
      kicker: kicker == freezed
          ? _value.kicker
          : kicker // ignore: cast_nullable_to_non_nullable
              as String?,
      contentKicker: contentKicker == freezed
          ? _value.contentKicker
          : contentKicker // ignore: cast_nullable_to_non_nullable
              as String?,
      printHeadline: printHeadline == freezed
          ? _value.printHeadline
          : printHeadline // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      seo: seo == freezed
          ? _value.seo
          : seo // ignore: cast_nullable_to_non_nullable
              as String?,
      sub: sub == freezed
          ? _value.sub
          : sub // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$HeadlineCopyWith<$Res> implements $HeadlineCopyWith<$Res> {
  factory _$HeadlineCopyWith(_Headline value, $Res Function(_Headline) then) =
      __$HeadlineCopyWithImpl<$Res>;
  @override
  $Res call(
      {String main,
      @JsonKey(defaultValue: "") String? kicker,
      @JsonKey(defaultValue: "", name: "content_kicker") String? contentKicker,
      @JsonKey(defaultValue: "", name: "print_headline") String? printHeadline,
      @JsonKey(defaultValue: "") String? name,
      @JsonKey(defaultValue: "") String? seo,
      @JsonKey(defaultValue: "") String? sub});
}

/// @nodoc
class __$HeadlineCopyWithImpl<$Res> extends _$HeadlineCopyWithImpl<$Res>
    implements _$HeadlineCopyWith<$Res> {
  __$HeadlineCopyWithImpl(_Headline _value, $Res Function(_Headline) _then)
      : super(_value, (v) => _then(v as _Headline));

  @override
  _Headline get _value => super._value as _Headline;

  @override
  $Res call({
    Object? main = freezed,
    Object? kicker = freezed,
    Object? contentKicker = freezed,
    Object? printHeadline = freezed,
    Object? name = freezed,
    Object? seo = freezed,
    Object? sub = freezed,
  }) {
    return _then(_Headline(
      main == freezed
          ? _value.main
          : main // ignore: cast_nullable_to_non_nullable
              as String,
      kicker == freezed
          ? _value.kicker
          : kicker // ignore: cast_nullable_to_non_nullable
              as String?,
      contentKicker == freezed
          ? _value.contentKicker
          : contentKicker // ignore: cast_nullable_to_non_nullable
              as String?,
      printHeadline == freezed
          ? _value.printHeadline
          : printHeadline // ignore: cast_nullable_to_non_nullable
              as String?,
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      seo == freezed
          ? _value.seo
          : seo // ignore: cast_nullable_to_non_nullable
              as String?,
      sub == freezed
          ? _value.sub
          : sub // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Headline implements _Headline {
  _$_Headline(
      this.main,
      @JsonKey(defaultValue: "") this.kicker,
      @JsonKey(defaultValue: "", name: "content_kicker") this.contentKicker,
      @JsonKey(defaultValue: "", name: "print_headline") this.printHeadline,
      @JsonKey(defaultValue: "") this.name,
      @JsonKey(defaultValue: "") this.seo,
      @JsonKey(defaultValue: "") this.sub);

  factory _$_Headline.fromJson(Map<String, dynamic> json) =>
      _$_$_HeadlineFromJson(json);

  @override
  final String main;
  @override
  @JsonKey(defaultValue: "")
  final String? kicker;
  @override
  @JsonKey(defaultValue: "", name: "content_kicker")
  final String? contentKicker;
  @override
  @JsonKey(defaultValue: "", name: "print_headline")
  final String? printHeadline;
  @override
  @JsonKey(defaultValue: "")
  final String? name;
  @override
  @JsonKey(defaultValue: "")
  final String? seo;
  @override
  @JsonKey(defaultValue: "")
  final String? sub;

  @override
  String toString() {
    return 'Headline(main: $main, kicker: $kicker, contentKicker: $contentKicker, printHeadline: $printHeadline, name: $name, seo: $seo, sub: $sub)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Headline &&
            (identical(other.main, main) ||
                const DeepCollectionEquality().equals(other.main, main)) &&
            (identical(other.kicker, kicker) ||
                const DeepCollectionEquality().equals(other.kicker, kicker)) &&
            (identical(other.contentKicker, contentKicker) ||
                const DeepCollectionEquality()
                    .equals(other.contentKicker, contentKicker)) &&
            (identical(other.printHeadline, printHeadline) ||
                const DeepCollectionEquality()
                    .equals(other.printHeadline, printHeadline)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.seo, seo) ||
                const DeepCollectionEquality().equals(other.seo, seo)) &&
            (identical(other.sub, sub) ||
                const DeepCollectionEquality().equals(other.sub, sub)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(main) ^
      const DeepCollectionEquality().hash(kicker) ^
      const DeepCollectionEquality().hash(contentKicker) ^
      const DeepCollectionEquality().hash(printHeadline) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(seo) ^
      const DeepCollectionEquality().hash(sub);

  @JsonKey(ignore: true)
  @override
  _$HeadlineCopyWith<_Headline> get copyWith =>
      __$HeadlineCopyWithImpl<_Headline>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_HeadlineToJson(this);
  }
}

abstract class _Headline implements Headline {
  factory _Headline(
      String main,
      @JsonKey(defaultValue: "") String? kicker,
      @JsonKey(defaultValue: "", name: "content_kicker") String? contentKicker,
      @JsonKey(defaultValue: "", name: "print_headline") String? printHeadline,
      @JsonKey(defaultValue: "") String? name,
      @JsonKey(defaultValue: "") String? seo,
      @JsonKey(defaultValue: "") String? sub) = _$_Headline;

  factory _Headline.fromJson(Map<String, dynamic> json) = _$_Headline.fromJson;

  @override
  String get main => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get kicker => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "content_kicker")
  String? get contentKicker => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "", name: "print_headline")
  String? get printHeadline => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get seo => throw _privateConstructorUsedError;
  @override
  @JsonKey(defaultValue: "")
  String? get sub => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$HeadlineCopyWith<_Headline> get copyWith =>
      throw _privateConstructorUsedError;
}
