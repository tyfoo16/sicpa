// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'headline.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Headline _$_$_HeadlineFromJson(Map<String, dynamic> json) {
  return _$_Headline(
    json['main'] as String,
    json['kicker'] as String? ?? '',
    json['content_kicker'] as String? ?? '',
    json['print_headline'] as String? ?? '',
    json['name'] as String? ?? '',
    json['seo'] as String? ?? '',
    json['sub'] as String? ?? '',
  );
}

Map<String, dynamic> _$_$_HeadlineToJson(_$_Headline instance) =>
    <String, dynamic>{
      'main': instance.main,
      'kicker': instance.kicker,
      'content_kicker': instance.contentKicker,
      'print_headline': instance.printHeadline,
      'name': instance.name,
      'seo': instance.seo,
      'sub': instance.sub,
    };
