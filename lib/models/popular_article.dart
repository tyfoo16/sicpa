import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sicpa/models/media.dart';

part 'popular_article.freezed.dart';
part 'popular_article.g.dart';

@freezed
class PopularArticle with _$PopularArticle {
  @JsonSerializable(explicitToJson: true)
  factory PopularArticle(
    String uri,
    String url,
    int id,
    @JsonKey(name: 'asset_id') int assetId,
    String source,
    @JsonKey(name: 'published_date') String publishedDate,
    String updated,
    String section,
    String subsection,
    String nytdsection,
    @JsonKey(name: 'adx_keywords') String adxKeywords,
    @deprecated String? column,
    String byline,
    String type,
    String title,
    @JsonKey(name: 'abstract') String abstractStr,
    @JsonKey(name: 'des_facet') List<String> desFacet,
    @JsonKey(name: 'org_facet') List<String> orgFacet,
    @JsonKey(name: 'per_facet') List<String> perFacet,
    @JsonKey(name: 'geo_facet') List<String> geoFacet,
    List<Media> media,
    @deprecated @JsonKey(name: 'eta_id') int etaId,
  ) = _PopularArticle;

  factory PopularArticle.fromJson(Map<String, dynamic> json) =>
      _$PopularArticleFromJson(json);
}
