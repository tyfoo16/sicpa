// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Article _$_$_ArticleFromJson(Map<String, dynamic> json) {
  return _$_Article(
    json['web_url'] as String? ?? '',
    json['snippet'] as String? ?? '',
    json['print_page'] as String? ?? '0',
    json['source'] as String? ?? '',
    (json['multimedia'] as List<dynamic>?)
            ?.map((e) => Multimedia.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
    Headline.fromJson(json['headline'] as Map<String, dynamic>),
    (json['keywords'] as List<dynamic>?)
            ?.map((e) => Keyword.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
    json['pub_date'] as String? ?? '',
    json['document_type'] as String? ?? '',
    json['news_desk'] as String? ?? '',
    Byline.fromJson(json['byline'] as Map<String, dynamic>),
    json['type_of_material'] as String? ?? '',
    json['_id'] as String? ?? '',
    json['word_count'] as int? ?? 0,
    json['score'] as int? ?? 0,
    json['uri'] as String? ?? '',
  );
}

Map<String, dynamic> _$_$_ArticleToJson(_$_Article instance) =>
    <String, dynamic>{
      'web_url': instance.webUrl,
      'snippet': instance.snippet,
      'print_page': instance.printPage,
      'source': instance.source,
      'multimedia': instance.multimedia,
      'headline': instance.headline,
      'keywords': instance.keywords,
      'pub_date': instance.pubDate,
      'document_type': instance.documentType,
      'news_desk': instance.newsDesk,
      'byline': instance.byline,
      'type_of_material': instance.typeOfMaterial,
      '_id': instance.id,
      'word_count': instance.wordCount,
      'score': instance.score,
      'uri': instance.uri,
    };
