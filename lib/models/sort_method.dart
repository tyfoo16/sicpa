enum SortMethod {
  Newest,
  Oldest,
  Relevance,
}

extension SortMethodExtension on SortMethod {
  String get value {
    switch (this) {
      case SortMethod.Newest:
        return "newest";
      case SortMethod.Oldest:
        return "oldest";
      case SortMethod.Relevance:
        return "relevance";
    }
  }
}
