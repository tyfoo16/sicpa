// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Media _$_$_MediaFromJson(Map<String, dynamic> json) {
  return _$_Media(
    json['type'] as String,
    json['subtype'] as String,
    json['caption'] as String,
    json['copyright'] as String,
    json['approved_for_syndication'] as int,
    (json['media-metadata'] as List<dynamic>)
        .map((e) => MediaMetadata.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$_MediaToJson(_$_Media instance) => <String, dynamic>{
      'type': instance.type,
      'subtype': instance.subtype,
      'caption': instance.caption,
      'copyright': instance.copyright,
      'approved_for_syndication': instance.approvedForSyndication,
      'media-metadata': instance.mediaMetadata,
    };
