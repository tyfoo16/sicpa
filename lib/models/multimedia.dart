//  {
//     "rank": 0,
//     "subtype": "xlarge",
//     "caption": null,
//     "credit": null,
//     "type": "image",
//     "url": "images/2021/09/23/t-magazine/23tmag-puffybags-slide-FQ2O-copy/23tmag-puffybags-slide-FQ2O-copy-articleLarge.jpg",
//     "height": 400,
//     "width": 600,
//     "legacy": {
//         "xlarge": "images/2021/09/23/t-magazine/23tmag-puffybags-slide-FQ2O-copy/23tmag-puffybags-slide-FQ2O-copy-articleLarge.jpg",
//         "xlargewidth": 600,
//         "xlargeheight": 400
//     },
//     "subType": "xlarge",
//     "crop_name": "articleLarge"
// },

import 'package:freezed_annotation/freezed_annotation.dart';

part 'multimedia.freezed.dart';
part 'multimedia.g.dart';

@freezed
class Multimedia with _$Multimedia {
  factory Multimedia(
    int rank,
    @JsonKey(defaultValue: "") String? subtype,
    @JsonKey(defaultValue: "") String? caption,
    @JsonKey(defaultValue: "") String? credit,
    @JsonKey(defaultValue: "") String? type,
    @JsonKey(defaultValue: "") String? url,
    @JsonKey(defaultValue: 0) int? height,
    @JsonKey(defaultValue: 0) int? width,
    // @JsonKey(defaultValue: {}, name: "legacy") Map<String, dynamic> legacy,
    @JsonKey(defaultValue: "", name: "crop_name") String? cropName,
  ) = _Multimedia;

  factory Multimedia.fromJson(Map<String, dynamic> json) =>
      _$MultimediaFromJson(json);
}
