// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'search_form.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SearchFormTearOff {
  const _$SearchFormTearOff();

  _SearchForm call(
      String? beginDate,
      String? endDate,
      bool? facet,
      FacetFields? facetFields,
      bool? facetFilter,
      String? fieldList,
      String? filterQuery,
      int? page,
      String query,
      SortMethod? sort) {
    return _SearchForm(
      beginDate,
      endDate,
      facet,
      facetFields,
      facetFilter,
      fieldList,
      filterQuery,
      page,
      query,
      sort,
    );
  }
}

/// @nodoc
const $SearchForm = _$SearchFormTearOff();

/// @nodoc
mixin _$SearchForm {
  String? get beginDate => throw _privateConstructorUsedError;
  String? get endDate => throw _privateConstructorUsedError;
  bool? get facet => throw _privateConstructorUsedError;
  FacetFields? get facetFields => throw _privateConstructorUsedError;
  bool? get facetFilter => throw _privateConstructorUsedError;
  String? get fieldList => throw _privateConstructorUsedError;
  String? get filterQuery => throw _privateConstructorUsedError;
  int? get page => throw _privateConstructorUsedError;
  String get query => throw _privateConstructorUsedError;
  SortMethod? get sort => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SearchFormCopyWith<SearchForm> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchFormCopyWith<$Res> {
  factory $SearchFormCopyWith(
          SearchForm value, $Res Function(SearchForm) then) =
      _$SearchFormCopyWithImpl<$Res>;
  $Res call(
      {String? beginDate,
      String? endDate,
      bool? facet,
      FacetFields? facetFields,
      bool? facetFilter,
      String? fieldList,
      String? filterQuery,
      int? page,
      String query,
      SortMethod? sort});
}

/// @nodoc
class _$SearchFormCopyWithImpl<$Res> implements $SearchFormCopyWith<$Res> {
  _$SearchFormCopyWithImpl(this._value, this._then);

  final SearchForm _value;
  // ignore: unused_field
  final $Res Function(SearchForm) _then;

  @override
  $Res call({
    Object? beginDate = freezed,
    Object? endDate = freezed,
    Object? facet = freezed,
    Object? facetFields = freezed,
    Object? facetFilter = freezed,
    Object? fieldList = freezed,
    Object? filterQuery = freezed,
    Object? page = freezed,
    Object? query = freezed,
    Object? sort = freezed,
  }) {
    return _then(_value.copyWith(
      beginDate: beginDate == freezed
          ? _value.beginDate
          : beginDate // ignore: cast_nullable_to_non_nullable
              as String?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String?,
      facet: facet == freezed
          ? _value.facet
          : facet // ignore: cast_nullable_to_non_nullable
              as bool?,
      facetFields: facetFields == freezed
          ? _value.facetFields
          : facetFields // ignore: cast_nullable_to_non_nullable
              as FacetFields?,
      facetFilter: facetFilter == freezed
          ? _value.facetFilter
          : facetFilter // ignore: cast_nullable_to_non_nullable
              as bool?,
      fieldList: fieldList == freezed
          ? _value.fieldList
          : fieldList // ignore: cast_nullable_to_non_nullable
              as String?,
      filterQuery: filterQuery == freezed
          ? _value.filterQuery
          : filterQuery // ignore: cast_nullable_to_non_nullable
              as String?,
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int?,
      query: query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      sort: sort == freezed
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as SortMethod?,
    ));
  }
}

/// @nodoc
abstract class _$SearchFormCopyWith<$Res> implements $SearchFormCopyWith<$Res> {
  factory _$SearchFormCopyWith(
          _SearchForm value, $Res Function(_SearchForm) then) =
      __$SearchFormCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? beginDate,
      String? endDate,
      bool? facet,
      FacetFields? facetFields,
      bool? facetFilter,
      String? fieldList,
      String? filterQuery,
      int? page,
      String query,
      SortMethod? sort});
}

/// @nodoc
class __$SearchFormCopyWithImpl<$Res> extends _$SearchFormCopyWithImpl<$Res>
    implements _$SearchFormCopyWith<$Res> {
  __$SearchFormCopyWithImpl(
      _SearchForm _value, $Res Function(_SearchForm) _then)
      : super(_value, (v) => _then(v as _SearchForm));

  @override
  _SearchForm get _value => super._value as _SearchForm;

  @override
  $Res call({
    Object? beginDate = freezed,
    Object? endDate = freezed,
    Object? facet = freezed,
    Object? facetFields = freezed,
    Object? facetFilter = freezed,
    Object? fieldList = freezed,
    Object? filterQuery = freezed,
    Object? page = freezed,
    Object? query = freezed,
    Object? sort = freezed,
  }) {
    return _then(_SearchForm(
      beginDate == freezed
          ? _value.beginDate
          : beginDate // ignore: cast_nullable_to_non_nullable
              as String?,
      endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String?,
      facet == freezed
          ? _value.facet
          : facet // ignore: cast_nullable_to_non_nullable
              as bool?,
      facetFields == freezed
          ? _value.facetFields
          : facetFields // ignore: cast_nullable_to_non_nullable
              as FacetFields?,
      facetFilter == freezed
          ? _value.facetFilter
          : facetFilter // ignore: cast_nullable_to_non_nullable
              as bool?,
      fieldList == freezed
          ? _value.fieldList
          : fieldList // ignore: cast_nullable_to_non_nullable
              as String?,
      filterQuery == freezed
          ? _value.filterQuery
          : filterQuery // ignore: cast_nullable_to_non_nullable
              as String?,
      page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int?,
      query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      sort == freezed
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as SortMethod?,
    ));
  }
}

/// @nodoc

class _$_SearchForm implements _SearchForm {
  _$_SearchForm(
      this.beginDate,
      this.endDate,
      this.facet,
      this.facetFields,
      this.facetFilter,
      this.fieldList,
      this.filterQuery,
      this.page,
      this.query,
      this.sort);

  @override
  final String? beginDate;
  @override
  final String? endDate;
  @override
  final bool? facet;
  @override
  final FacetFields? facetFields;
  @override
  final bool? facetFilter;
  @override
  final String? fieldList;
  @override
  final String? filterQuery;
  @override
  final int? page;
  @override
  final String query;
  @override
  final SortMethod? sort;

  @override
  String toString() {
    return 'SearchForm(beginDate: $beginDate, endDate: $endDate, facet: $facet, facetFields: $facetFields, facetFilter: $facetFilter, fieldList: $fieldList, filterQuery: $filterQuery, page: $page, query: $query, sort: $sort)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SearchForm &&
            (identical(other.beginDate, beginDate) ||
                const DeepCollectionEquality()
                    .equals(other.beginDate, beginDate)) &&
            (identical(other.endDate, endDate) ||
                const DeepCollectionEquality()
                    .equals(other.endDate, endDate)) &&
            (identical(other.facet, facet) ||
                const DeepCollectionEquality().equals(other.facet, facet)) &&
            (identical(other.facetFields, facetFields) ||
                const DeepCollectionEquality()
                    .equals(other.facetFields, facetFields)) &&
            (identical(other.facetFilter, facetFilter) ||
                const DeepCollectionEquality()
                    .equals(other.facetFilter, facetFilter)) &&
            (identical(other.fieldList, fieldList) ||
                const DeepCollectionEquality()
                    .equals(other.fieldList, fieldList)) &&
            (identical(other.filterQuery, filterQuery) ||
                const DeepCollectionEquality()
                    .equals(other.filterQuery, filterQuery)) &&
            (identical(other.page, page) ||
                const DeepCollectionEquality().equals(other.page, page)) &&
            (identical(other.query, query) ||
                const DeepCollectionEquality().equals(other.query, query)) &&
            (identical(other.sort, sort) ||
                const DeepCollectionEquality().equals(other.sort, sort)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(beginDate) ^
      const DeepCollectionEquality().hash(endDate) ^
      const DeepCollectionEquality().hash(facet) ^
      const DeepCollectionEquality().hash(facetFields) ^
      const DeepCollectionEquality().hash(facetFilter) ^
      const DeepCollectionEquality().hash(fieldList) ^
      const DeepCollectionEquality().hash(filterQuery) ^
      const DeepCollectionEquality().hash(page) ^
      const DeepCollectionEquality().hash(query) ^
      const DeepCollectionEquality().hash(sort);

  @JsonKey(ignore: true)
  @override
  _$SearchFormCopyWith<_SearchForm> get copyWith =>
      __$SearchFormCopyWithImpl<_SearchForm>(this, _$identity);
}

abstract class _SearchForm implements SearchForm {
  factory _SearchForm(
      String? beginDate,
      String? endDate,
      bool? facet,
      FacetFields? facetFields,
      bool? facetFilter,
      String? fieldList,
      String? filterQuery,
      int? page,
      String query,
      SortMethod? sort) = _$_SearchForm;

  @override
  String? get beginDate => throw _privateConstructorUsedError;
  @override
  String? get endDate => throw _privateConstructorUsedError;
  @override
  bool? get facet => throw _privateConstructorUsedError;
  @override
  FacetFields? get facetFields => throw _privateConstructorUsedError;
  @override
  bool? get facetFilter => throw _privateConstructorUsedError;
  @override
  String? get fieldList => throw _privateConstructorUsedError;
  @override
  String? get filterQuery => throw _privateConstructorUsedError;
  @override
  int? get page => throw _privateConstructorUsedError;
  @override
  String get query => throw _privateConstructorUsedError;
  @override
  SortMethod? get sort => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$SearchFormCopyWith<_SearchForm> get copyWith =>
      throw _privateConstructorUsedError;
}
