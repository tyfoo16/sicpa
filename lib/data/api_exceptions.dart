class UnauthorizedException implements Exception {
  @override
  String toString() {
    return "Unauthorized";
  }
}

class NotFoundException implements Exception {
  @override
  String toString() {
    return "404 Not Found";
  }
}

class InternalServerErrorException implements Exception {
  @override
  String toString() {
    return "Internal Server Error";
  }
}

class BadRequestException implements Exception {
  @override
  String toString() {
    return "Bad Request";
  }
}

class NetworkException implements Exception {
  @override
  String toString() {
    return "Network Exception";
  }
}

class NoInternetException implements Exception {
  NoInternetException();

  @override
  String toString() {
    return "No internet connection";
  }
}

class RequestException implements Exception {
  final String message;

  RequestException(this.message);

  @override
  String toString() {
    return this.message;
  }
}

class TooManyRequestException implements Exception {
  final String message;

  TooManyRequestException(this.message);

  @override
  String toString() {
    return this.message;
  }
}
