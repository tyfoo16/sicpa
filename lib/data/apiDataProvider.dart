import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sicpa/data/constants.dart';

import 'api_exceptions.dart';

class ApiDataProvider {
  final String baseUrl = Configs.baseUrl;
  final String apiKey = Configs.apiKey;

  final http.Client client;

  ApiDataProvider(this.client);

  http.Response returnOrThrow(http.Response response) {
    print("Status Code: ${response.statusCode}");
    final status = response.statusCode;

    switch (status) {
      case 200:
        return response;
      case 400:
        throw BadRequestException();
      case 401:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException();
      case 429:
        throw TooManyRequestException(
            "Too many requests.  You reached your per minute or per day rate limit.");
      case 500:
        throw InternalServerErrorException();
      default:
        throw RequestException("Request failed: $status");
    }
  }

  Future<http.Response> get(String url) async {
    var responseJson;
    try {
      Map<String, dynamic> params = {
        "api-key": apiKey,
      };
      final uri = Uri.https("api.nytimes.com", url, params);
      print(uri.toString());
      final response = await client.get(uri);
      responseJson = returnOrThrow(response);
      return responseJson;
    } on SocketException {
      throw NoInternetException();
    }
  }

  Future<http.Response> getWithParams(
      String url, Map<String, dynamic> params) async {
    var responseJson;
    try {
      params["api-key"] = apiKey;
      final uri = Uri.https(baseUrl, url, params);

      final response = await client.get(uri);
      responseJson = returnOrThrow(response);
      return responseJson;
    } on SocketException {
      throw NoInternetException();
    }
  }
}
